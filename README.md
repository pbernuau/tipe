TIPE 2010 - 2011 : Conception d'un moteur physique
=====================

- - - 

Présentation
-------------------------------

Ce projet présente le programme développé dans le cadre des TIPE[^TIPE] 2010 - 2011 sur le thème de la
**mobilité** et du **mouvement**.

L'objectif du programme que j'ai réalisé était d'illustrer les techniques qui peuvent être
utilisées lors de l'implémentation d'un *moteur physique*.

Principales caractériques :

- Le moteur implémenté est capable de gérer les intéractions entre des boules (mobiles) et des
triangles (statiques).
- L'intégration des équations du mouvement exploite la méthode de Runge-Kutta.
- La détection des collisions est optimisée grâce à l'utilisation d'un *octree*
- Les simulations sont initialisées à partir de scripts JavaScript.

Langages et bibliothèques utilisées :

- C++, JavaScript
- Qt 4.8
- OpenGL

Liens complémentaires :

- [Présentation du projet (PDF)](https://bitbucket.org/pbernuau/tipe/raw/master/documents/Présentation.pdf)
- [Illustrations](https://bitbucket.org/pbernuau/tipe/raw/master/screenshots/)
- [Fiche synoptique](https://bitbucket.org/pbernuau/tipe/raw/master/documents/Fiche synoptique.md)

Compilation et exécution
--------------------------------------

Compilation (nécessite *qmake* et *make*) :
```
#!bash
qmake
make all
```

Exécution :
```
#!bash
# Unix :
./release/TIPE
# Windows :
.\release\TIPE.exe
```

[^TIPE]: [Travail d'initiative personnelle encadré](http://fr.wikipedia.org/wiki/Travail_d'initiative_personnelle_encadré)