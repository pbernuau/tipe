
// Présentation des boules

for (var i = 0; i < 4; i++)
{
	var c = [ $, $, $ ];
	CreateBall({
		position: 	[100, i*75, 5],
		radius:		10,
		color:		c
	});
	CreateBall({
		position: 	[140, i*75, 10],
		radius:		20,
		color:		c
	});

	CreateBall({
		position: 	[200, i*75, 25],
		radius:		35,
		color:		c
	});
}


