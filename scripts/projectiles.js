
var triangle =
{
  vertices:
  [
    [ -0.5*1000,  0.8660*1000, -5 ],
    [ -0.5*1000, -0.8660*1000, -5 ],
    [ 1000, 0, -5 ]
  ],
  elasticity:   0.5,
  color: [ 0.15, 0.42, 0.60 ]
};
CreateTriangle(triangle);

var v = 50, pi = Math.PI;

var alpha_1 = pi/6.0, alpha_2 = pi/4.0, alpha_3 = pi/3.0;

var ball =
{
  position: 	  [0, 20, 0],
  velocity:	    [0, v*Math.cos(alpha_1), v*Math.sin(alpha_1)],
  elasticity:	  0.5,
  radius:		    5,
  color:        [ 0.4, 0.6, 0.8 ],
  mass:         15
};
CreateBall(ball);

ball.position[0] += 50;
ball.velocity = [0, v*Math.cos(alpha_2), v*Math.sin(alpha_2)],
ball.color = [ 0.6, 0.8, 0.4 ];
CreateBall(ball);

ball.position[0] += 50;
ball.velocity = [0, v*Math.cos(alpha_3), v*Math.sin(alpha_3)],
ball.color = [ 0.8, 0.4, 0.4 ];
CreateBall(ball);