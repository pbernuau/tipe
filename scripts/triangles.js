__defineGetter__("$", Math.random);

// Présentation des triangles

for (var i = 0; i < 4; i++)
{
	var c = [ $, $, $ ];
	
	CreateTriangle({
		vertices:
		[
			[0, i*75, i*10],
			[0, i*75+10, 0],
			[10, i*75+10, 0]		
		],
		color: c
	});
	
	CreateTriangle({
		vertices:
		[
			[40, i*75, i*10],
			[40, i*75+20, 0],
			[60, i*75+20, 0]		
		],
		color: c
	});
	
	CreateTriangle({
		vertices:
		[
			[100, i*75, i*10],
			[100, i*75+35, 0],
			[135, i*75+35, 0]		
		],
		color: c
	});
}
