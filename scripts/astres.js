__defineGetter__("$", Math.random);
__defineGetter__("$$$", function () { return [$, $, $]; });

function Length(x, y, z)
{
    return Math.sqrt(x*x+y*y+z*z);
}

function Normalize(x, y, z)
{
    var l = Length(x, y, z);
    
    if (l == 0)
        return [0,0,0];
    else
        return [x/l,y/l,z/l];
}

function CreateSpringForce (origin, l0, k)
{
  return (function ()
  {
    var dp =
    [
      [ origin[0] - this.position[0] ],
      [ origin[1] - this.position[1] ],
      [ origin[2] - this.position[2] ]
    ];
    
    var l = Math.sqrt(dp[0]*dp[0]+dp[1]*dp[1]+dp[2]*dp[2]);
    var n = [ dp[0]/l, dp[1]/l, dp[2]/l ];
    n[0] *= k * (l-l0);
    n[1] *= k * (l-l0);
    n[2] *= k * (l-l0);
    
    return n;
  });
}

function forcecentrale ()
{
  var ur = Normalize(this.position[0], this.position[1], 0);
  var r = Length(this.position[0], this.position[1], 0);
  var k = 0.020;
  
  return [-k*r*ur[0], -k*r*ur[1], -k*r*ur[2]];
}

var ball =
{
  position: [50, 0, 0],
  velocity: [0, 50, 0],
  callback: forcecentrale,
  radius: 5,
  mass: 0.1,
  constantforce: [0,0,0],
  color: [1, 0.4, 0.4]
};
CreateBall(ball);

var ball =
{
  position: [-30, 0, -0],
  velocity: [0, 10, 0],
  callback: forcecentrale,
  radius: 4,
  mass: 0.05,
  constantforce: [0,0,0],
  color: [0.4, 0.4, 1]
};
CreateBall(ball);

