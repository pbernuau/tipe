
var Map = {};
this.Map = Map;

//(function () {

const WORLD_LIMIT = 4096;
const EPSILON = 1e-3;

//=============================================================================
// Point
//=============================================================================
function Point (x, y, z)
{
	this.x = parseFloat(x),
	this.y = parseFloat(y),
	this.z = parseFloat(z);
}

Point.ORIGIN = new Point(0, 0, 0);

Point.prototype.copy = function ()
{
	var pt = new Point;
	return pt.set(this);
};

Point.prototype.set = function (pt)
{
	this.x = pt.x;
	this.y = pt.y;
	this.z = pt.z;
	return this;
};

Point.prototype.length = function ()
{
	return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
};

Point.prototype.isOutside = function ()
{
	return	Math.abs(this.x) > WORLD_LIMIT ||
			Math.abs(this.y) > WORLD_LIMIT ||
			Math.abs(this.z) > WORLD_LIMIT;
};

//
// Swaps Y and Z coordinates (the point is modified)
//
Point.prototype.swapYZ = function ()
{
	var y = this.y;
	this.y = this.z;
	this.y = y;
	return this;
};

Point.prototype.add = function (pt)
{
	this.x += pt.x;
	this.y += pt.y;
	this.z += pt.z;
	return this;
};

Point.prototype.substract = function (pt)
{
	this.x -= pt.x;
	this.y -= pt.y;
	this.z -= pt.z;
	return this;
};

Point.prototype.scale = function (factor)
{
	this.x *= factor;
	this.y *= factor;
	this.z *= factor;
	return this;
};

Point.prototype.normalize = function ()
{
	var length = this.length();
	
	if (Math.abs(length) < EPSILON) {
		return this.set(Point.ORIGIN);
	}
	
	return this.scale(1.0/length);	
};

Point.prototype.crossProduct = function (pt)
{
	var result = new Point;
	result.x = this.y * pt.z - pt.y * this.z;
	result.y = this.z * pt.x - pt.z * this.x;
	result.z = this.x * pt.y - pt.x * this.y;
	return result;
};

Point.prototype.dotProduct = function (pt)
{
	return this.x*pt.x + this.y*pt.y + this.z*pt.z;
};

Point.prototype.toString = function ()
{
	console.warn("Point converted to string.");
	return this.x + " " + this.y + " " + this.z;
};

Point.sum = function (points)
{
	if (arguments.length != 1) {
		console.warn("Invalid use of Point.sum detected.");
	}
	
	var result = new Point(0, 0, 0);
	for (var i = 0; i < points.length; i++)
	{
		var pt = points[i];
		if (pt instanceof Point) {
			result.add(pt);
		}
	}
	
	return result;	
};

Point.substract = function (pt1, pt2)
{
	return pt1.copy().substract(pt2);
};

Point.scale = function (pt, factor)
{
	return pt.copy().scale(factor);
};

Point.getBarycenter = function (points)
{
	return Point.scale(Point.sum(points), points.length);
};

//=============================================================================
// Plane
//=============================================================================
function Plane (pt1, pt2, pt3)
{
	// creates the plane from 3 memberpoints
	this.normal = Point.substract(pt2, pt1).crossProduct(
		Point.substract(pt3, pt1));
	this.normal.normalize();
	
	this.d = -pt1.dotProduct(this.normal);
}

Plane.BACK 		= {};
Plane.FRONT		= {};
Plane.PLANAR	= {};

Plane.prototype.classifyPointRelation = function (point)
{
	var dist = this.normal.dotProduct(point) + this.d;
	
	if (dist < -EPSILON) {
		return Plane.BACK;
	}
	
	if (dist > EPSILON) {
		return Plane.FRONT;
	}
	
	return Plane.PLANAR;
};

Plane.prototype.getIntersectionWithPlanes = function (o1, o2)
{
	var line = this.getIntersectionWithPlane(o1);
	if (line.length) {
		return o2.getIntersectionWithLine(line[0], line[1]);
	}
	
	return null;
};

// returns [point, vector] or [] if there is no intersection
Plane.prototype.getIntersectionWithPlane = function(other)
{
	var fn00 = this.normal.length();
	var fn01 = this.normal.dotProduct(other.normal);
	var fn11 = other.normal.length();
	var det = fn00*fn11 - fn01*fn01;
	
	if (Math.abs(det) < EPSILON) {
		return [];
	}
	
	var invdet = 1.0 / det;
	var fc0 = (fn11*-this.d + fn01*other.d) * invdet;
	var fc1 = (fn00*-other.d + fn01*this.d) * invdet;
	
	var point = Point.scale(this.normal, fc0);
	point.add(Point.scale(other.normal, fc1));
	
	var vector = this.normal.crossProduct(other.normal);
	
	return [point, vector];
};

Plane.prototype.getIntersectionWithLine = function(linePoint, lineVect)
{
	var t2 = this.normal.dotProduct(lineVect);
	
	if (Math.abs(t2) < EPSILON) {
		return null;
	}
	
	var t = -(this.normal.dotProduct(linePoint) + this.d) / t2;
	return Point.sum([linePoint, Point.scale(lineVect, t)]);
};

//=============================================================================
function BuildCameraLookAtMatrixLH (position, target, up)
{
	var zaxis = Point.substract(target, position);
	zaxis.normalize();
	
	var xaxis = up.crossProduct(zaxis);
	xaxis.normalize();
	
	var yaxis = zaxis.crossProduct(xaxis);
	
	var M = [];
	M[0] = xaxis.x;
	M[1] = yaxis.x;
	M[2] = zaxis.x;
	M[3] = 0.0;

	M[4] = xaxis.y;
	M[5] = yaxis.y;
	M[6] = zaxis.y;
	M[7] = 0.0;

	M[8] = xaxis.z;
	M[9] = yaxis.z;
	M[10] = zaxis.z;
	M[11] = 0.0;

	M[12] = -xaxis.dotProduct(position);
	M[13] = -yaxis.dotProduct(position);
	M[14] = -zaxis.dotProduct(position);
	M[15] = 1.0;
	
	return M;
}

function TransformVector (M, vect)
{
	var result = new Point;
	result.x = vect.x*M[0] + vect.y*M[4] + vect.z*M[8] + M[12];
	result.y = vect.x*M[1] + vect.y*M[5] + vect.z*M[9] + M[13];
	result.z = vect.x*M[2] + vect.y*M[6] + vect.z*M[10] + M[14];
	return result;
}

function ComputeConvexHull (cloud)
{
	cloud.sort(function (u, v) {
		return u.x - v.x;
	});
	
	var startPoint = cloud.shift();
	
	cloud.sort(function (u, v) {
		var tta_a = Math.atan2(u.y - startPoint.y, u.x - startPoint.x);
		var tta_b = Math.atan2(v.y - startPoint.y, v.x - startPoint.x);
		
		return tta_b - tta_a;	
	});
	
	var hull = [  ];
	
	cloud.push(startPoint);
	
	while (cloud.length)
	{
		hull.push(cloud.shift());
		while (hull.length >= 4)
		{
			var n = hull.length - 1;
			
			var b = hull[n];
			var p2 = hull[n-1];
			var p1 = hull[n-2];
			var a = hull[n-3];
			
			var line =
			{
				a: p2.y - p1.y,
				b: -p2.x + p1.x,
				c: p1.y*p2.x - p1.x*p2.y
			};
			
			if ((line.a * a.x + line.b * a.y + line.c) *
				(line.a * b.x + line.b * b.y + line.c) < 0.0) {
				hull = ArrayErase(hull, n-1);
			}
			else {
				break;
			}
		}
	}
	return hull;
}

function ArrayErase (array, n)
{
	var copy = array.slice();
	var result = [];
	
	for (var i = 0; i < array.length; i++)
	{
		if (i != n) {
			result.push(copy.shift());
		}
		else {
			copy.shift();
		}	
	}
	
	return result;
}

//=============================================================================

function Load (buffer)
{
	var faces = [];
	var lines = buffer.split("\n");
	var data = [];
	
	for (var i = 0; i < lines.length; i++)
	{
		if (lines[i][0] == '(') {
			data.push(ParseLine(lines[i]));
		}
		else if (lines[i][0] == '}') {
			faces = faces.concat(CompileData(data));
			data = [];
		}
	}
	
	return faces;
}

//=============================================================================
// ParseLine
//=============================================================================
function ParseLine (line)
{
	var parts = line.split(" ");
	var values = [];
	for (var i = 0; i < parts.length; i++)
	{
		var f = parseFloat(parts[i]);
		if (!isNaN(f)) {
			values.push(f);
		}
	}
	
	var data =
	{
		vertices:
		[
			new Point( values[0], values[1], values[2] ), // vertex 1
			new Point( values[3], values[4], values[5] ), // vertex 2
			new Point( values[6], values[7], values[8] )  // vertex 3
		],
		textureCoordinates:
		{
			u: 			new Point( values[9], values[10], values[11] ),	// x / y / z
			v: 			new Point( values[13], values[14], values[15] ),
			shift: 		[ values[12], values[16] ],				// shift x / y
			rotation:	values[17],
			scale: 		[ values[18], values[19] ]				// scale x / y
		},
		textureName: line.match(/.+\) (\w+) \[.+\]/)[1]
	};
	
	return data;
}

//=============================================================================
// CompileData
//=============================================================================
function CompileData (data)
{
	var planes = [];
	var faces = [];
	
	for (var i = 0; i < data.length; i++)
	{
		planes.push(new Plane(data[i].vertices[0],
							  data[i].vertices[1],
							  data[i].vertices[2]));
	}
	
	for (var i = 0; i < planes.length; i++)
	{
		var face =
		{
			vertices: 	[],
			texture:	data[i].textureName,
			special:	false
		};
		
		var coplanarPoints = [];
		var vertices = [];
		for (var j = 0; j < planes.length; j++)
		{
			if (i == j)
				continue;
			
			for (var k = j + 1; k < planes.length; k++)
			{
				if (i == k)
					continue;
				
				var point = planes[i].getIntersectionWithPlanes(planes[j], planes[k]);
				if (point == null)
					continue;
				
				if (point.isOutside())
					continue;
				
				for (var m = 0; m < planes.length && point != null; m++)
				{
					if (i == m || m == j || m == k)
						continue;
					
					if (planes[m].classifyPointRelation(point) == Plane.BACK) {
						point = null;
					}
				}
				
				if (point != null) {
					coplanarPoints.push(point);
				}
			}
		}
		
		if (coplanarPoints.length < 3)
			continue;
		
		// We use the barycenter to build a projection matrix.
		var barycenter = Point.getBarycenter(coplanarPoints);
		
		// probl�me : le barycentre n'est que le barycentre d'une face.
		// il faut regarder si toutes les faces sont AAATRIGGER
		// ou alors au moins une enfin bref donc voil�.
		
		/*if (face.texture == "AAATRIGGER")
		{
			face.special = true;
			face.position = barycenter.copy();
			face.radius = data[i].textureCoordinates.scale[0];
			
			faces.push(face);
			continue;
		}*/
		
		var position = Point.sum([barycenter, planes[i].normal]);
		var up = Point.substract(coplanarPoints[1], coplanarPoints[0]);
		var mat = BuildCameraLookAtMatrixLH(position, barycenter, up);
		delete up;
											
		// Builds the cloud point
		// (Graham's algorithme is usable in 2d)
		var cloud = [];
		for (var j = 0; j < coplanarPoints.length; j++)
		{
			var transformed = TransformVector(mat, coplanarPoints[j]);
			transformed.basePoint = coplanarPoints[j];
			cloud.push(transformed);
		}
		
		// Sorts the cloud
		var hull = ComputeConvexHull(cloud);
		for (var j = 0; j < hull.length; j++) {
			vertices.push(hull[j].basePoint);
		}
		delete cloud;
		
		// Computes the volume's barycenter
		//barycenter = Point.getBarycenter(vertices);
		
		/*var texturePlane = new Plane(new Point(0, 0, 0),
									data[i].textureCoordinates.u,
									data[i].textureCoordinates.v);*/
									
		for (var j = 0; j < vertices.length; j++)
		{
			face.vertices.push(vertices[j].swapYZ());
			face.normal = planes[i].normal;
			
			// Computes texture coordinates
			/*vector3df temp, proj = texturePlane.Normal;
			proj.normalize();
			proj *= texturePlane.getDistanceTo(vertices[j]);
			proj *= texturePlane.isFrontFacing(vertices[j]) ? 1.f : -1.f;
			proj += vertices[j];

			// u and v are supposed to be normalised in the file
			temp.X = data[i].u.dotProduct(proj) / data[i].scaleX + data[i].shiftX;
			temp.Y = data[i].v.dotProduct(proj) / data[i].scaleY + data[i].shiftY;

			if (texture)
			{
				temp.X /= texture->getSize().Width;
				temp.Y /= texture->getSize().Height;
			}

			vertex.TCoords.X = temp.X;
			vertex.TCoords.Y = temp.Y;*/
		}
    
    if (face.texture != "AAATRIGGER") {
      faces.push(face);
    }
	}
	
	return faces;
}

Map.LoadFromBuffer = Load;
Map.Point = Point;

//})();
