///////////////////////////////////////////////////////////////////////////////
// Test avanc� 2
// Choc parfaitement �lastique entre deux sph�res identiques,
// l'une en mouvement et l'autre immobile.
// La vitesse est enti�rement transmise apr�s le choc.
///////////////////////////////////////////////////////////////////////////////

var ball1 =
{
  position: 	  [-50, 0, 0],
  velocity:	    [20, 0, 0],
  elasticity:	  1.0,
  radius:		    5,
  color:        [ 0, 1, 0 ],
  constantforce: [0,0,0]
};
CreateBall(ball1);

var ball2 =
{
  position: 	  [0, 0, 0],
  velocity:	    [0, 0, 0],
  elasticity:	  1.0,
  radius:		    5,
  color:        [ 0, 0, 1 ],
  constantforce: [0,0,0]
};
CreateBall(ball2);
