///////////////////////////////////////////////////////////////////////////////
// Test avanc� 6
// 360 boules dispos�es sur 9 colonnes verticales sont lach�es avec une
// vitesse initiale al�atoire sur un plan horizontal.
// Les boules ont des rayons al�atoires entre 3 et 7.
///////////////////////////////////////////////////////////////////////////////

__defineGetter__("$", Math.random);
__defineGetter__("$$$", function () { return [$, $, $]; });

Include("scripts/MAP.js");

var ball =
{
  velocity:	    [0, 0, 0],
  elasticity:	  0.8,
  radius:		    5,
};

var map = Map.LoadFromBuffer(ReadFile("media/plane.map"));
Print(map.length + " faces loaded.");

for (var i = 0; i < map.length; i++)
{
  var color = [0.5, 0.6, 1.0];
  
	if (map[i].special) {
		continue;
	}
	
	var vertices = map[i].vertices;
	for (var j = 1; j < vertices.length - 1; j++)
	{
		var tri =
		{
			elasticity: 1.0,
			color: color,
			vertices:
			[
				[ vertices[j].x/8.0, vertices[j].y/8.0, vertices[j].z/8.0 ],
				[ vertices[0].x/8.0, vertices[0].y/8.0, vertices[0].z/8.0 ],
				[ vertices[j+1].x/8.0, vertices[j+1].y/8.0, vertices[j+1].z/8.0 ]
			],
      immaterial: j != 1
		};
		
		CreateTriangle(tri);
	}
}

for (var x = 0; x < 3; x++)
{
  for (var y = 0; y < 3; y++)
  {
    for (var i = 1; i < 80; i++)
    {
      ball.position = [$-1+x*20, $-1+y*20, 20*i];
      ball.velocity = [$-1, $-1, $-1];
      ball.color = $$$;
      ball.radius = 3 + $*4;
      CreateBall(ball);
    }
  }
}

