///////////////////////////////////////////////////////////////////////////////
// Test de base 5
// Sph�re pos�e sur un triangle horizontal lanc�e vers un triangle inclin�
// verticalement (90�).
// La vitesse initiale est petite et l�g�rement "d�vi�e".
// (la sph�re d�crit ainsi un triangle)
///////////////////////////////////////////////////////////////////////////////


var ball =
{
  position: 	  [0, 0, 5],
  velocity:	    [-5, 2, 0],
  elasticity:	  0.9,
  radius:		    5,
  color:        [ 0, 1, 0 ]
};
CreateBall(ball);

var triangle =
{
  vertices:
  [
    [ -0.5*50,  0.8660*50, 0 ],
    [ -0.5*50, -0.8660*50, 0 ],
    [ 50, 0, 0 ]
  ],
  elasticity:   0.9,
  color: [ 0.7, 0.7, 0.7 ]
};
CreateTriangle(triangle);

triangle.vertices =
[
  [ -0.5*50, 0.8660*50, 0 ],
  [ -0.5*50, 0, 50 ],
  [ -0.5*50, -0.8660*50, 0 ]
];
triangle.color = [ 0.8, 0.8, 0.8 ];
CreateTriangle(triangle);

