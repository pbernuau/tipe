///////////////////////////////////////////////////////////////////////////////
// Test avanc� 1
// Sph�re envoy�e dans un demi-cylindre avec une �lasticit� nulle.
///////////////////////////////////////////////////////////////////////////////

__defineGetter__("$", Math.random);

Include("scripts/MAP.js");

var ball =
{
  position: 	  [0, 0, 4],
  velocity:	    [50, 0, 0],
  elasticity:	  0,
  mass:         1,
  friction:     0.2,
  radius:		    4,
  color:        [ 0, 1, 0 ],
  initialforce: [0,0,10*9.81]
};
CreateBall(ball);


var map = Map.LoadFromBuffer(ReadFile("media/tank.map"));
Print(map.length + " faces loaded.");

for (var i = 0; i < map.length; i++)
{
  var color = [$,$,$];
  
	if (map[i].special) {
		continue;
	}
	
	var vertices = map[i].vertices;
	for (var j = 1; j < vertices.length - 1; j++)
	{
		var tri =
		{
			elasticity: 1.0,
			color: color,
			vertices:
			[
				[ vertices[j].x/8.0, vertices[j].y/8.0, vertices[j].z/8.0 ],
				[ vertices[0].x/8.0, vertices[0].y/8.0, vertices[0].z/8.0 ],
				[ vertices[j+1].x/8.0, vertices[j+1].y/8.0, vertices[j+1].z/8.0 ]
			],
      immaterial: j != 1
		};
		
		CreateTriangle(tri);
	}
}
