///////////////////////////////////////////////////////////////////////////////
// Test avanc� 3
// Chocs dans un cube.
///////////////////////////////////////////////////////////////////////////////

__defineGetter__("$", Math.random);

Include("scripts/MAP.js");

var map = Map.LoadFromBuffer(ReadFile("media/cube.map"));
Print(map.length + " faces loaded.");

for (var i = 0; i < map.length; i++)
{
  var color = [$,$,$];
  
	if (map[i].special) {
		continue;
	}
	
	var vertices = map[i].vertices;
	for (var j = 1; j < vertices.length - 1; j++)
	{
		var tri =
		{
			elasticity: 1.0,
			color: color,
			vertices:
			[
				[ vertices[j].x, vertices[j].y, vertices[j].z ],
				[ vertices[0].x, vertices[0].y, vertices[0].z ],
				[ vertices[j+1].x, vertices[j+1].y, vertices[j+1].z ]
			],
      immaterial: j != 1
		};
		
		CreateTriangle(tri);
	}
}

for (var j = 0; j < 10; j++)
{
  var ball =
  {
    position: 	  [($-1)*50, ($-1)*50, ($-1)*50],
    velocity:	    [($-1)*20, ($-1)*20, ($-1)*20],
    elasticity:	  $,
    radius:		    ($+1)*20,
    color:        [ $, $, $ ],
    constantforce: [0,0,0]
  };
  CreateBall(ball);
  
  Print("Ball created.");
}
