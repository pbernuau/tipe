///////////////////////////////////////////////////////////////////////////////
// Test de base 1
// Sph�re l�ch�e sans vitesse initiale sur un triangle horizontal
///////////////////////////////////////////////////////////////////////////////

var ball =
{
  position: 	  [0, 0, 50],
  velocity:	    [0, 0, 0],
  elasticity:	  0.8,
  radius:		    5,
  color:        [ Math.random(), Math.random(), Math.random() ]
};

var triangle =
{
  vertices:
  [
    [ -0.5*50,  0.8660*50, 0 ],
    [ -0.5*50, -0.8660*50, 0 ],
    [ 50, 0, 0 ]
  ],
  elasticity:   0.7,
  color: [ Math.random(), Math.random(), Math.random() ]
};

CreateBall(ball);
CreateTriangle(triangle);

