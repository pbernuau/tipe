///////////////////////////////////////////////////////////////////////////////
// Test de base 2
// Sph�re l�ch�e sans vitesse initiale sur un triangle inclin�
// (et une tr�s faible �lasticit�)
///////////////////////////////////////////////////////////////////////////////


var ball =
{
  position: 	  [0, 0, 6],
  velocity:	    [0, 0, 0],
  elasticity:	  0.5,
  radius:		    5,
  color:        [ 1, 0, 0 ]
};

var triangle =
{
  vertices:
  [
    [ -0.5*50,  0.8660*50, 0 ],
    [ -0.5*50, -0.8660*50, 0 ],
    [ 50, 0, -25 ]
  ],
  elasticity:   0.5,
  color: [ 0.15, 0.42, 0.60 ]
};

CreateBall(ball);
CreateTriangle(triangle);

triangle.vertices =
[
    [ -0.5*50,  0.8660*50, -25 ],
    [ -0.5*50, -0.8660*50, -25 ],
    [ 50, 0, -25 ]
]
triangle.color = [ 0.8, 0.8, 0.8 ];
triangle.immaterial = true;
CreateTriangle(triangle);