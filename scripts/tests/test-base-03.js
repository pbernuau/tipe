///////////////////////////////////////////////////////////////////////////////
// Test de base 3
// Sph�re l�ch�e avec une vitesse initiale verticale montant vers un triangle
// inclin�.
///////////////////////////////////////////////////////////////////////////////


var ball =
{
  position: 	  [0, 0, -25],
  velocity:	    [0, 0, 50],
  elasticity:	  0.9,
  radius:		    5,
  color:        [ Math.random(), Math.random(), Math.random() ]
};

var triangle =
{
  vertices:
  [
    [ -0.5*50, -0.8660*50, 0 ],
    [ -0.5*50,  0.8660*50, 0 ],
    [ 50, 0, 0 ]
  ],
  elasticity:   0.9,
  color: [ Math.random(), Math.random(), Math.random() ]
};

CreateBall(ball);
CreateTriangle(triangle);

