///////////////////////////////////////////////////////////////////////////////
// Test avanc� 4
// Mini billard.
// Cas int�ressant : lancer la boule blanche sans composante selon y (ligne 56)
// La variable z permet de cr�er plusieurs couches pour v�rifier la fluidit�.
///////////////////////////////////////////////////////////////////////////////

__defineGetter__("$", Math.random);

var coordinates = [
  [ 0, 0 ],
  [ 10, 5 ], [ 10, -5 ],
  [ 20, 0 ], [ 20, 10 ], [ 20, -10 ],
  [ 30, 5 ], [ 30, 15 ], [ 30, -5 ], [ 30, -15 ]
];

var colors = [
  [ 253, 239, 18 ],
  [ 241, 14, 41 ],
  [ 180, 32, 44 ],
  [ 0.2, 0.2, 0.2 ],
  [ 180, 32, 44 ],
  [ 0, 28, 161 ],
  [ 23, 115, 66 ],
  [ 241, 14, 41 ],
  [ 252, 53, 125 ],
  [ 23, 115, 66 ],
  [ 38, 18, 98 ],
  [ 0, 28, 161 ], 
  [ 38, 18, 98 ],
  [ 253, 239, 18 ],
  [ 252, 53, 125 ]
];

for (var z = 0; z < 100; z += 100) {

var ball =
{
  position:     [0,0, z],
  velocity:	    [0, 0, 0],
  elasticity:	  0.9,
  radius:		    5,
  initialforce: [0,0, 9.81]
};

for (var i = 0; i < coordinates.length; i++)
{
  ball.position[0] = coordinates[i][0];
  ball.position[1] = coordinates[i][1];
  ball.color = [ colors[i][0]/255.0, colors[i][1]/255.0, colors[i][2]/255.0 ];
  
  CreateBall(ball);
}

ball.position = [ -100, 0, z ];
ball.velocity = [ 50, ($-1.0), 0 ];
ball.color = [ 1.0, 1.0, 1.0 ];
CreateBall(ball);

var triangle =
{
  vertices:
  [
    [ -500, -500, z-5 ],
    [ 500,  -500, z-5 ],
    [ 0, 500, z-5 ]
  ],
  elasticity:   1.0,
  color: [ 0.2, 0.5, 0.2 ]
};
CreateTriangle(triangle);

}
