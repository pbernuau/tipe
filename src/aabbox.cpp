#include <algorithm>

#include "aabbox.h"
#include "globals.h"
#include "mymath.h"

// Component-wise comparison
static bool LesserOrEqual(const vec3& u, const vec3& v)
{
  return (u.x < v.x || Equals(u.x, v.x))
      && (u.y < v.y || Equals(u.y, v.y))
      && (u.z < v.z || Equals(u.z, v.z));
}

static bool Lesser(const vec3& u, const vec3& v)
{
  return (u.x < v.x) && (u.y < v.y) && (u.z < v.z);
}

AABBox::AABBox(const AABBox& box) :
  _min_edge(box._min_edge),
  _max_edge(box._max_edge)
{
}

AABBox::AABBox(const vec3& point) :
  _min_edge(point),
  _max_edge(point)
{
  Repair();
}

AABBox::AABBox(float size) :
  _min_edge(-size, -size, -size),
  _max_edge(size, size, size)
{
  Repair();
}

AABBox::AABBox(const vec3& point1, const vec3& point2) :
  _min_edge(point1),
  _max_edge(point2)
{
  Repair();
}

AABBox::AABBox(const vec3& point1, const vec3& point2, const vec3& point3) :
  _min_edge(point1),
  _max_edge(point2)
{
  Repair();
  AddInternalPoint(point3);
}

void AABBox::Assign(const AABBox& box)
{
  _min_edge = box._min_edge;
  _max_edge = box._max_edge;
}

void AABBox::AddInternalPoint(const vec3& point) {
  AddInternalPoint(point.x, point.y, point.z);
}

void AABBox::AddInternalPoint(float x, float y, float z)
{
  if (x > _max_edge.x) _max_edge.x = x;
  if (y > _max_edge.y) _max_edge.y = y;
  if (z > _max_edge.z) _max_edge.z = z;

  if (x < _min_edge.x) _min_edge.x = x;
  if (y < _min_edge.y) _min_edge.y = y;
  if (z < _min_edge.z) _min_edge.z = z;
}

void AABBox::AddInternalBox(const AABBox& box)
{
  AddInternalPoint(box._min_edge);
  AddInternalPoint(box._max_edge);
}

void AABBox::Translate(const vec3& translation)
{
  _min_edge += translation;
  _max_edge += translation;
}

void AABBox::Reset(const vec3& point)
{
  _min_edge = point;
  _max_edge = point;
}

void AABBox::Repair()
{
  if (_min_edge.x > _max_edge.x)
  {
    std::swap(_min_edge.x, _max_edge.x);
  }

  if (_min_edge.y > _max_edge.y)
  {
    std::swap(_min_edge.y, _max_edge.y);
  }

  if (_min_edge.z > _max_edge.z)
  {
    std::swap(_min_edge.z, _max_edge.z);
  }
}

bool AABBox::Equals(const AABBox& box) const {
  return (_min_edge == box._min_edge) && (box._max_edge == _max_edge);
}

vec3 AABBox::GetCenter() const {
  return (_min_edge + _max_edge) / 2.f;
}

vec3 AABBox::GetExtent() const {
  return _max_edge - _min_edge;
}

void AABBox::GetVertices(vec3* vertices) const
{
  CHECK_POINTER(vertices);

  const vec3 middle = GetCenter();
  const vec3 diag = middle - _max_edge;

  /*
    Edges are stored in this way:
       /3--------/7
      / |       / |
     /  |      /  |
    1---------5   |
    |  /2- - -|- -6
    | /       |  /
    |/        | /
    0---------4/
  */

  vertices[0] = vec3(middle.x + diag.x, middle.y + diag.y, middle.z + diag.z);
  vertices[1] = vec3(middle.x + diag.x, middle.y - diag.y, middle.z + diag.z);
  vertices[2] = vec3(middle.x + diag.x, middle.y + diag.y, middle.z - diag.z);
  vertices[3] = vec3(middle.x + diag.x, middle.y - diag.y, middle.z - diag.z);
  vertices[4] = vec3(middle.x - diag.x, middle.y + diag.y, middle.z + diag.z);
  vertices[5] = vec3(middle.x - diag.x, middle.y - diag.y, middle.z + diag.z);
  vertices[6] = vec3(middle.x - diag.x, middle.y + diag.y, middle.z - diag.z);
  vertices[7] = vec3(middle.x - diag.x, middle.y - diag.y, middle.z - diag.z);
}

bool AABBox::IsEmpty() const {
  return _min_edge == _max_edge;
}

bool AABBox::IsPointInside(const vec3& point) const {
  return LesserOrEqual(_min_edge, point) && LesserOrEqual(point, _max_edge);
}

bool AABBox::IsPointTotalInside(const vec3& point) const {
  return Lesser(_min_edge, point) && Lesser(point, _max_edge);
}

bool AABBox::IsFullInside(const AABBox& box) const
{
  return LesserOrEqual(box._min_edge, _min_edge)
      && LesserOrEqual(_max_edge, box._max_edge);
}

bool AABBox::IntersectsWithBox(const AABBox& box) const {
  return  LesserOrEqual(_min_edge, box._max_edge) &&
          LesserOrEqual(box._min_edge, _max_edge);
}

void AABBox::Shrink(float value)
{
  static const vec3 u(1.f, 1.f, 1.f);
  _min_edge += u*value;
  _max_edge -= u*value;
  Repair();
}
