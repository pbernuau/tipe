#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qmainwindow.h>

class GLWidget;

//
// MainWindow
//
// Displays the menus and the main central GLWidget.
//
class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow();
  ~MainWindow();

private:
  GLWidget* _central_widget;
};

#endif // MAINWINDOW_H
