#include <qgl.h>

#include "drawaxes.h"

#include <GL/gl.h>
#include <GL/glu.h>

void DrawAxes(float length, const vec3 origin)
{
  glTranslatef(origin.x, origin.y, origin.z);
  glScalef(length, length, length);

  glColor3f(1.f, 0.f, 0.f);
  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(1, 0, 0);
  glEnd();

  glColor3f(0.f, 1.f, 0.f);
  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(0, 1, 0);
  glEnd();

  glColor3f(0.f, 0.f, 1.f);
  glBegin(GL_LINES);
  glVertex3i(0, 0, 0);
  glVertex3i(0, 0, 1);
  glEnd();

  glScalef(1.f/length, 1.f/length, 1.f/length);
  glTranslatef(-origin.x, -origin.y, -origin.z);
}

