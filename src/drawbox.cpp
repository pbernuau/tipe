#include <qgl.h>

#include "aabbox.h"
#include "drawbox.h"

#include <GL/gl.h>
#include <GL/glu.h>

void DrawBox(const AABBox& box, float r, float g, float b)
{
  static int i[] = {
    0, 1, 5, 4,
    2, 3, 7, 6,
  };

  vec3 vertices[8];
  box.GetVertices(&vertices[0]);

  /*
    Edges are stored in this way:
       /3--------/7
      / |       / |
     /  |      /  |
    1---------5   |
    |  /2- - -|- -6
    | /       |  /
    |/        | /
    0---------4/
  */

  glColor3f(r, g, b);

  glBegin(GL_LINE_LOOP);
  for (int j = 0; j < 4; j++) {
    glVertex3f(vertices[i[j]].x, vertices[i[j]].y, vertices[i[j]].z);
  }
  glEnd();

  glBegin(GL_LINE_LOOP);
  for (int j = 0; j < 4; j++) {
    glVertex3f(vertices[i[j]+2].x, vertices[i[j]+2].y, vertices[i[j]+2].z);
  }
  glEnd();

  for (int j = 0; j < 4; j++)
  {
    glBegin(GL_LINES);
    glVertex3f(vertices[i[j]].x, vertices[i[j]].y, vertices[i[j]].z);
    glVertex3f(vertices[i[j]+2].x, vertices[i[j]+2].y, vertices[i[j]+2].z);
    glEnd();
  }
}
