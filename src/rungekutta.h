#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

namespace RungeKutta
{

// y' = f(t, y)
template<
  typename State,			// type of 'y'
  typename Functor>		// type of 'f'
State Integrate(
  const State& yn,		// current state
  float tn,           // current time
  float dt,           // time step
  Functor& fn)  // function 'f'
{
  State k1 = fn(tn, yn);
  State k2 = fn(tn + dt / 2.f, yn + dt * k1 / 2.f);
  State k3 = fn(tn + dt / 2.f, yn + dt * k2 / 2.f);
  State k4 = fn(tn + dt, yn + dt * k3);

  return yn + (k1 + 2.f * k2 + 2.f * k3 + k4) * dt / 6.f;
}

} // namespace RungeKutta

#endif // RUNGEKUTTA_H
