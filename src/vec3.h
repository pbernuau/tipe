#ifndef VEC3_H
#define VEC3_H

#include <QMetaType>

//
// vec3
//
// 3-dimensional vector.
//
class vec3
{
public:
  vec3();
  vec3(float x, float y, float z);
  vec3(const vec3& v);
  vec3(const vec3& from, const vec3& to);

  vec3& operator+=(const vec3& v);
  vec3	operator+(const vec3& v) const;

  vec3& operator-=(const vec3& v);
  vec3	operator-(const vec3& v) const;
  vec3	operator-() const;

  vec3& operator*=(const float a);
  vec3	operator*(const float a) const;

  float operator*(const vec3& v) const;

  vec3& operator/=(const float a);
  vec3	operator/(const float a) const;

  bool operator==(const vec3& v) const;
  bool operator!=(const vec3& v) const;

  float Dot(const vec3& v) const;

  vec3	Cross(const vec3& v) const;

  float Length() const;

  vec3& Normalize();
  vec3	Normalized() const;

  bool IsNull() const;

  float x;
  float y;
  float z;

private:
  friend vec3 operator*(const float a, const vec3& v);
};

vec3 operator*(const float a, const vec3& v);

#include "vec3-inl.h"

Q_DECLARE_METATYPE(vec3)

#endif // VEC3_H
