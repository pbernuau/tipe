#include "mymath.h"
#include "vec3.h"

bool vec3::operator==(const vec3& v) const {
  return Equals(x, v.x) && Equals(y, v.y) && Equals(z, v.z);
}

bool vec3::operator!=(const vec3& v) const {
  return !Equals(x, v.x) || !Equals(y, v.y) || !Equals(z, v.z);
}

float vec3::Dot(const vec3& v) const {
  return x*v.x + y*v.y + z*v.z;
}

vec3 vec3::Cross(const vec3& v) const {
  return vec3( y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x );
}

float vec3::Length() const {
  return Sqrt(Dot(*this));
}

vec3& vec3::Normalize()
{
  float length = Length();

  if (::IsNull(length))
    x = y = z = 0.f;
  else
    (*this) /= length;

  return *this;
}

vec3 vec3::Normalized() const
{
  float length = Length();

  if (::IsNull(length))
    return vec3();
  else
    return (*this) / length;
}

bool vec3::IsNull() const
{
  return ::IsNull(x) && ::IsNull(y) && ::IsNull(z);
}
