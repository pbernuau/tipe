#include <qgl.h>

#include "ball.h"
#include "drawbox.h"
#include "mymath.h"

#include <GL/gl.h>
#include <GL/glu.h>

Ball::Ball(float mass, float radius, float elasticity, float friction) :
  SceneItem(elasticity, friction),
  _mass(mass),
  _radius(radius),
  _data(NULL),
  _is_colliding(false),
  _collision_delta_position(INFINITE),
  _apply_force(NULL)
{
}

Ball::~Ball()
{
  if (_data != NULL) {
    _data->Drop();
  }
}

bool Ball::IsValid() const
{
  return SceneItem::IsValid()
      && !IsNull(_mass) && _mass > 0.f
      && !IsNull(_radius) && _radius > 0.f;
}

AABBox Ball::BoundingBox() const
{
  AABBox box(_radius), new_box(_radius);
  new_box.Translate(_new_position);
  box.Translate(_position);
  box.AddInternalBox(new_box);
  return box;
}

bool Ball::IsMoveable() const {
  return true;
}

const vec3& Ball::Position() const {
  return _position;
}

void Ball::SetPosition(const vec3& position) {
  _position = position;
}

const vec3& Ball::Velocity() const {
  return _velocity;
}

void Ball::SetVelocity(const vec3& velocity) {
  _velocity = velocity;
}

const vec3& Ball::Acceleration() const {
  return _acceleration;
}

void Ball::SetAcceleration(const vec3& acceleration) {
  _acceleration = acceleration;
}

const vec3& Ball::NewPosition() const {
  return _new_position;
}

void Ball::SetNewPosition(const vec3& position) {
  _new_position = position;
}

const vec3& Ball::NewVelocity() const {
  return _new_velocity;
}

void Ball::SetNewVelocity(const vec3& velocity) {
  _new_velocity = velocity;
}

const vec3& Ball::NewAcceleration() const {
  return _new_acceleration;
}

void Ball::SetNewAcceleration(const vec3& acceleration) {
  _new_acceleration = acceleration;
}

float Ball::Mass() const {
  return _mass;
}

float Ball::Radius() const {
  return _radius;
}

ReferenceCounted* Ball::Data() const {
  return _data;
}

void Ball::SetData(ReferenceCounted* data)
{
  if (_data != NULL) {
    _data->Drop();
  }

  _data = data;

  if (_data != NULL) {
    _data->Grab();
  }
}

bool Ball::IsColliding() const {
  return _is_colliding;
}

const vec3& Ball::CollisionPosition() const {
  return _collision_position;
}

void Ball::SetCollisionPosition(const vec3& position) {
  _collision_position = position;
}

const vec3& Ball::CollisionVelocity() const {
  return _collision_velocity;
}

float Ball::CollisionDeltaPosition() const {
  return _collision_delta_position;
}

void Ball::SetCollision(const vec3& velocity, float delta)
{
  _collision_velocity = velocity;
  _collision_delta_position = delta;
  _is_colliding = true;
}

void Ball::ClearCollisionData()
{
  _collision_velocity = vec3();
  _collision_delta_position = INFINITE;
  _is_colliding = false;
}

void Ball::PushCollisionPoint(const vec3& point) {
  _collision_points.push_back(point);
}

void Ball::PopCollisionPoint() {
  _collision_points.pop_back();
}


ApplyForceFunc Ball::ApplyForceCallback() const {
  return _apply_force;
}

void Ball::SetApplyForceCallback(ApplyForceFunc callback) {
  _apply_force = callback;
}

const vec3& Ball::ConstantResultantForce() const {
  return _constant_resultant_force;
}

void Ball::SetConstantResultantForce(const vec3& force) {
  _constant_resultant_force = force;
}

const vec3& Ball::VariableResultantForce() const {
  return _variable_resultant_force;
}

void Ball::AddVariableForce(const vec3& force) {
  _variable_resultant_force += force;
}

void Ball::ClearVariableResultantForce() {
  _variable_resultant_force = vec3();
}

void Ball::Render(int debug_data) const
{
  GLUquadric* quadric = gluNewQuadric();
  glPushMatrix();

  float color[4] = { 0.f };
  Color(&color[0], &color[1], &color[2]);

  glTranslatef(_position.x, _position.y, _position.z);

  double r = _radius < 1.f ? 1.0 : static_cast<double>(_radius);

  glEnable(GL_LIGHTING);
  glColor3f(1.f, 1.f, 1.f);
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
  gluSphere(quadric, r, 10, 10);
  glDisable(GL_LIGHTING);

  if (debug_data & DEBUG_DATA_NET_FORCE)
  {
    vec3 net_force = VariableResultantForce() + ConstantResultantForce();

    glDisable(GL_DEPTH_TEST);
    glColor3f(1.f, 0.f, 0.f);
    glBegin(GL_LINES);
    glVertex3f(0.f, 0.f, 0.f);
    glVertex3f(net_force.x, net_force.y, net_force.z);
    glEnd();
    glEnable(GL_DEPTH_TEST);
  }

  if (debug_data & DEBUG_DATA_VELOCITY)
  {
    glDisable(GL_DEPTH_TEST);
    glColor3f(0.f, 1.f, 0.f);
    glBegin(GL_LINES);
    glVertex3f(0.f, 0.f, 0.f);
    glVertex3f(_velocity.x, _velocity.y, _velocity.z);
    glEnd();
    glEnable(GL_DEPTH_TEST);
  }

  glPopMatrix();
  gluDeleteQuadric(quadric);

  if (debug_data & DEBUG_DATA_BOUNDING_BOX)
  {
    AABBox box(_radius);
    box.Translate(_position);
    DrawBox(box, color[0], color[1], color[2]);
  }

  if (debug_data & DEBUG_DATA_COLLISION_POINTS)
  {
    glBegin(GL_POINTS);
    foreach (const vec3& point, _collision_points)
    {
      glColor3f(color[0], color[1], color[2]);
      glVertex3f(point.x, point.y, point.z);
    }
    glEnd();
  }
}

