#ifndef MYMATH_H
#define MYMATH_H

//
// MyMath
//
// Defines basic functions that manipulate float values
// and that are rounding-error aware.
//


// Returns true if x == y using a rounding-error epsilon.
bool Equals(float x, float y);

// Returns true if x == 0 using a rounding-error epsilon.
bool IsNull(float x);

// Provided for convenience
float Sqrt(float x);
float Abs(float x);

#endif // MYMATH_H
