#include "octree.h"

static QPair<SceneItem*, SceneItem*> MakeCollisionPair(SceneItem* item1,
                                                       SceneItem* item2)
{
  if (item1 < item2) {
    return qMakePair(item1, item2);
  }
  else {
    return qMakePair(item2, item1);
  }
}

int SceneNode::AddItem(SceneItem* item)
{
  if (!_box.IntersectsWithBox(item->BoundingBox())) {
    return 0;
  }

  if (IsLeaf())
  {
    GrabItem(item);
    return 1;
  }
  else
  {
    int count = 0;
    foreach (SceneNode* node, _children)
    {
      count += node->AddItem(item);
    }

    return count;
  }
}

void SceneNode::RemoveItem(SceneItem* item)
{
  if (IsLeaf())
  {
    DropItem(item);
  }
  else
  {
    foreach (SceneNode* node, _children) {
      node->RemoveItem(item);
    }
  }
}

int SceneNode::UpdateStructure()
{
  if (!IsLeaf())
  {
    int item_count = 0;
    foreach (SceneNode* node, _children) {
      item_count += node->UpdateStructure();
    }

    if (item_count < kMinItemsPerNode)
    {
      QSet<SceneItem*> static_items, dynamic_items;
      GetAllItems(static_items, dynamic_items);
      Reset();

      _static_items = static_items;
      _dynamic_items = dynamic_items;

      foreach (SceneItem* item, _static_items) {
        item->Grab();
      }
      foreach (SceneItem* item, _dynamic_items) {
        item->Grab();
      }

      item_count = _static_items.size() + _dynamic_items.size();
    }

    return item_count;
  }
  else if (NeedsSubdivision())
  {
    vec3 center = _box.GetCenter();
    vec3 vertices[8];

    _box.GetVertices(&vertices[0]);

    for (int i = 0; i < 8; i++)
    {
      AABBox box(center, vertices[i]);
      SceneNode* child = new SceneNode(this, box);
      CHECK_POINTER(child);

      _children.append(child);
    }

    foreach (SceneItem* item, _static_items)
    {
      bool added = AddItem(item) > 0;
      assert(added);
      item->Drop();
    }

    foreach (SceneItem* item, _dynamic_items)
    {
      bool added = AddItem(item) > 0;
      assert(added);
      item->Drop();
    }

    _static_items.clear();
    _dynamic_items.clear();

    return UpdateStructure();
  }

  return _static_items.size() + _dynamic_items.size();
}

void SceneNode::GetCollisions(
    QSet< QPair<SceneItem*, SceneItem*> >& collisions) const
{
  if (IsLeaf())
  {
    foreach (SceneItem* item, _dynamic_items)
    {
      AABBox box(item->BoundingBox());

      foreach (SceneItem* static_item, _static_items)
      {
        if (!static_item->Immaterial() &&
            box.IntersectsWithBox(static_item->BoundingBox()))
        {
          collisions.insert(MakeCollisionPair(item, static_item));
        }
      }

      foreach (SceneItem* dynamic_item, _dynamic_items)
      {
        if (dynamic_item == item) {
          continue;
        }

        if (box.IntersectsWithBox(dynamic_item->BoundingBox())) {
          collisions.insert(MakeCollisionPair(item, dynamic_item));
        }
      }
    }
  }
  else
  {
    foreach (SceneNode* child, _children) {
      child->GetCollisions(collisions);
    }
  }
}

void SceneNode::GetBoundingItems(const AABBox& box,
                                 QSet<SceneItem*>& items) const
{
  if (!box.IntersectsWithBox(_box)) {
    return;
  }

  if (IsLeaf())
  {
    foreach (SceneItem* item, _static_items)
    {
      if (box.IntersectsWithBox(item->BoundingBox())) {
        items.insert(item);
      }
    }
  }
  else
  {
    foreach (SceneNode* child, _children) {
      child->GetBoundingItems(box, items);
    }
  }
}

void SceneNode::Apply(ApplyFn fn, ApplyMode mode) const
{
  if (mode == AM_LEAVES_ONLY && IsLeaf())
  {
    fn(this);
  }
  else
  {
    if (mode == AM_ROOT_FIRST) {
      fn(this);
    }

    foreach (SceneNode* child, _children) {
      child->Apply(fn, mode);
    }

    if (mode == AM_LEAVES_FIRST) {
      fn(this);
    }
  }
}

void SceneNode::Reset()
{
  foreach (SceneItem* item, _static_items) {
    item->Drop();
  }

  foreach (SceneItem* item, _dynamic_items) {
    item->Drop();
  }

  // We can't use qDeleteAll because the SceneNode's destructor is protected
  foreach (SceneNode* node, _children) {
    delete node;
  }

  _static_items.clear();
  _dynamic_items.clear();
  _children.clear();
}

void SceneNode::UnmarkItems()
{
  if (IsLeaf())
  {
    foreach (SceneItem* item, _static_items|_dynamic_items) {
      item->Unmark();
    }
  }
  else
  {
    foreach (SceneNode* child, _children) {
      child->UnmarkItems();
    }
  }
}

const AABBox& SceneNode::Box() const {
  return _box;
}

bool SceneNode::IsLeaf() const {
  return _children.empty();
}

bool SceneNode::HasDynamicItems() const
{
  if (IsLeaf())
  {
    return !_dynamic_items.empty();
  }
  else
  {
    foreach (SceneNode* child, _children)
    {
      if (child->HasDynamicItems()) {
        return true;
      }
    }
  }

  return false;
}


void SceneNode::GetAllItems(QSet<SceneItem*>& static_items,
                            QSet<SceneItem*>& dynamic_items) const
{
  static_items.unite(_static_items);
  dynamic_items.unite(_dynamic_items);

  foreach (SceneNode* child, _children) {
    child->GetAllItems(static_items, dynamic_items);
  }
}

SceneNode::SceneNode(SceneNode* parent, const AABBox& box) :
  _box(box),
  _parent(parent)
{
}

SceneNode::~SceneNode()
{
  Reset();
}

bool SceneNode::NeedsSubdivision() const
{
  return  (_static_items.size() + _dynamic_items.size()) > kMaxItemsPerNode &&
          _box.GetExtent().Length() > kMinNodeSize;
}

void SceneNode::TriggerBubbling()
{
  if (IsLeaf())
  {
    QSet<SceneItem*> items = _dynamic_items;
    _dynamic_items.clear();

    foreach (SceneItem* item, items) {
      BubbleUp(item);
      item->Drop();
    }
  }
  else
  {
    foreach (SceneNode* node, _children) {
      node->TriggerBubbling();
    }
  }
}

void SceneNode::BubbleUp(SceneItem* item)
{
  if (item->IsMarked()) {
    return;
  }

  if (item->BoundingBox().IsFullInside(_box))
  {
    GrabItem(item);
    item->Mark();
  }
  else if (_parent != NULL)
  {
    _parent->BubbleUp(item);
  }
  else
  {
    item->Mark();
  }
}


void SceneNode::BubbleDown()
{
  if (!IsLeaf())
  {
    foreach (SceneItem* item, _dynamic_items)
    {
      foreach (SceneNode* node, _children)
      {
        node->AddItem(item);
      }

      item->Drop();
    }

    _dynamic_items.clear();

    foreach (SceneNode* node, _children) {
      node->BubbleDown();
    }
  }
}

void SceneNode::GrabItem(SceneItem* item)
{
  if (item->IsMoveable())
  {
    if (!_dynamic_items.contains(item))
    {
      _dynamic_items.insert(item);
      item->Grab();
    }
  }
  else
  {
    if (!_static_items.contains(item))
    {
      _static_items.insert(item);
      item->Grab();
    }
  }
}

void SceneNode::DropItem(SceneItem* item)
{
  if (item->IsMoveable())
  {
    if (_dynamic_items.contains(item))
    {
      _dynamic_items.remove(item);
      item->Drop();
    }
  }
  else
  {
    if (_static_items.contains(item))
    {
      _static_items.remove(item);
      item->Drop();
    }
  }
}
