#include "sceneitem.h"

SceneItem::SceneItem(float elasticity, float friction) :
  _elasticity(elasticity),
  _friction(friction),
  _immaterial(false),
  _name(-1),
  _red(0.8f),
  _green(0.8f),
  _blue(0.8f),
  _marked(false)
{
}

bool SceneItem::IsValid() const
{
  return _elasticity >= 0.f && _elasticity <= 1.f;
}

bool SceneItem::IsMoveable() const
{
  return false;
}

void SceneItem::Mark() {
  _marked = true;
}

void SceneItem::Unmark() {
  _marked = false;
}

bool SceneItem::IsMarked() const {
  return _marked;
}

float SceneItem::Elasticity() const {
  return _elasticity;
}

void SceneItem::SetElasticity(float elasticity) {
  _elasticity = elasticity;
}

float SceneItem::Friction() const {
  return _friction;
}

void SceneItem::SetFriction(float friction) {
  _friction = friction;
}

int SceneItem::Name() const {
  return _name;
}

void SceneItem::SetName(int name) {
  _name = name;
}

void SceneItem::Color(float* red, float* green, float* blue) const
{
  if (red != NULL) {
    *red = _red;
  }

  if (green != NULL) {
    *green = _green;
  }

  if (blue != NULL) {
    *blue = _blue;
  }
}

void SceneItem::SetColor(float red, float green, float blue)
{
  _red = red;
  _green = green;
  _blue = blue;
}

void SceneItem::Render(int) const {}

bool SceneItem::Immaterial() const {
  return _immaterial;
}

void SceneItem::SetImmaterial(bool immaterial) {
  _immaterial = immaterial;
}

