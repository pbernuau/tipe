#include <algorithm>

#include <qgl.h>
#include <qpair.h>

#include "ball.h"
#include "drawbox.h"
#include "engine.h"
#include "mymath.h"
#include "rungekutta.h"
#include "sceneitem.h"
#include "triangle.h"

#include <GL/gl.h>
#include <GL/glu.h>

void DrawNode(const SceneNode* node)
{
  AABBox box = node->Box();

  if (node->HasDynamicItems())
  {
    box.Shrink(kLineWidth / 2.f);
    DrawBox(box, 0.f, 0.f, 1.f);
  }
  else
  {
    DrawBox(box, 0.8f, 0.8f, 1.f);
  }
}

Engine::Engine() :
  _previous_render_mode(-1),
  _reset_display_list(true)
{
}

Engine::~Engine()
{
  Reset();
}

bool Engine::AddBall(Ball* ball)
{
  if (!ball->IsValid()) {
    return false;
  }

  if (_octree.AddItem(ball))
  {
    ball->Grab();
    _balls.append(ball);
    _items.append(ball);
    _octree.UpdateStructure();
  }

  return true;
}

bool Engine::AddTriangle(Triangle* triangle)
{
  if (!triangle->IsValid()) {
    return false;
  }

  if (_octree.AddItem(triangle))
  {
    triangle->Grab();
    _triangles.append(triangle);
    _items.append(triangle);

    _octree.UpdateStructure();

    _reset_display_list = true;

    return true;
  }

  return false;
}

int Engine::DeleteItems(int name)
{
  QList<SceneItem*> delete_list;
  foreach (SceneItem* item, _items)
  {
    if (item->Name() == name) {
      delete_list.append(item);
    }
  }

  int count = 0;
  foreach (SceneItem* item, delete_list) {
    count += DeleteItem(item);
  }

  return count;
}

int Engine::DeleteItem(SceneItem* item)
{
  int count = _items.removeAll(item);
  _balls.removeAll(dynamic_cast<Ball*>(item));
  _triangles.removeAll(dynamic_cast<Triangle*>(item));
  _octree.RemoveItem(item);

  if (count > 0) {
    item->Drop();
  }
  return count;
}

void Engine::Reset()
{
  foreach (SceneItem* item, _items) {
    item->Drop();
  }

  _items.clear();
  _balls.clear();
  _triangles.clear();
  _octree.Reset();

  _reset_display_list = true;
}

void Engine::Animate(float dt)
{
  ApplyForces(dt);

  // Moveable objects are move to their new nodes.
  _octree.UpdateMoveableItems();
  _octree.UpdateStructure();

  // Let's drop the items that escaped from the world.
  // We can skip the triangles because they don't move.
  foreach (Ball* ball, _balls)
  {
    // If there is a single reference to the item, it means that
    // the engine is the only owner of that object, so it is no longer
    // contained by any other node.
    if (ball->ReferenceCount() == 1) {
      DeleteItem(ball);
    }
  }

  QSet< QPair<SceneItem*, SceneItem*> > collisions;
  _octree.GetCollisions(collisions);

  QPair<SceneItem*, SceneItem*> pair;
  foreach (pair, collisions)
  {
    if (pair.first->IsMoveable())
    {
      if (pair.second->IsMoveable())
      {
        ComputeBallBallCollision(dynamic_cast<Ball*>(pair.first),
                                 dynamic_cast<Ball*>(pair.second));
      }
      else
      {
        ComputeBallTriangleCollision(dynamic_cast<Ball*>(pair.first),
                                     dynamic_cast<Triangle*>(pair.second));
      }
    }
    else
    {
      ComputeBallTriangleCollision(dynamic_cast<Ball*>(pair.second),
                                   dynamic_cast<Triangle*>(pair.first));
    }
  }

  foreach (Ball* ball, _balls)
  {
    ball->SetPosition(ball->NewPosition());
    ball->SetVelocity(ball->NewVelocity());
    ball->SetAcceleration(ball->NewAcceleration());
  }
}

void Engine::Render(int debug_data)
{
  if (debug_data & DEBUG_DATA_NO_DRAW) {
    return;
  }

  if (_previous_render_mode == debug_data && !_reset_display_list)
  {
    glCallList(1);
  }
  else
  {
    glNewList(1, GL_COMPILE_AND_EXECUTE);

    foreach (Triangle* triangle, _triangles) {
      triangle->Render(debug_data);
    }

    glEndList();

    _reset_display_list = false;
  }

  foreach (Ball* ball, _balls) {
    ball->Render(debug_data);
  }

  if (debug_data & DEBUG_DATA_OCTREE) {
    _octree.Apply(DrawNode, SceneNode::AM_LEAVES_ONLY);
  }

  _previous_render_mode = debug_data;
}

void Engine::ApplyForces(float dt)
{
  foreach (Ball *ball, _balls)
  {
    if (ball->IsColliding())
    {
      ball->SetVelocity(ball->CollisionVelocity());
      ball->SetPosition(ball->CollisionPosition());

      ball->ClearCollisionData();
    }


    State current_state;
    current_state.Velocity = ball->Velocity();
    current_state.Position = ball->Position();

    State final_state = RungeKutta::Integrate(current_state, 0.f, dt, *ball);

    ball->SetNewVelocity(final_state.Velocity);
    ball->SetNewPosition(final_state.Position);

    ball->ClearVariableResultantForce();
  }
}

#define DECOMPOSE(vec, n) \
  vec3 normal_ ## vec = n.Dot(vec) * n; \
  vec3 tangential_ ## vec = vec - normal_ ## vec

void Engine::ComputeBallTriangleCollision(Ball* ball, Triangle* triangle)
{
  const vec3& A = triangle->Vertex(0);
  const vec3& n = triangle->Normal();
  const float radius = ball->Radius();

  const float elasticity = Sqrt(ball->Elasticity() * triangle->Elasticity());
  const float friction = ball->Friction() * triangle->Friction();

  float previous_distance = (ball->Position() - A) * n;
  float new_distance = (ball->NewPosition() - A) * n;

  if (previous_distance < 0.f)
  {
    previous_distance = -previous_distance;
    return;
  }

  if (!Equals(new_distance, radius) && new_distance > radius) {
    return;
  }

  // The deplacement is decomposed into a tangential deplacement
  // and a normal deplacement.
  // The tangential deplacement remains unchanged.
  // The normal deplacement is cut into two parts :
  // - the amount of move until the ball touches the triangle
  // - the remaining amout of move
  // The latter is mirrored so that we keep as much move as possible
  // (to eschew loss of energy)
  vec3 dp = ball->NewPosition() - ball->Position();
  DECOMPOSE(dp, n);

  vec3 move_until_collide = (radius - previous_distance) * n;
  vec3 reflected_move = -(normal_dp - move_until_collide);
  vec3 final_position =
      ball->Position() + tangential_dp + move_until_collide + reflected_move;

  // The collision point is the point, on the triangle,
  // where the collision occurs.
  vec3 collision_point =
      ball->Position() + dp - (reflected_move + normal_dp) - n * radius;

  vec3 velocity = ball->IsColliding() ?
                  ball->CollisionVelocity() :
                  ball->NewVelocity();
  DECOMPOSE(velocity, n);
  vec3 final_velocity = tangential_velocity - normal_velocity * elasticity;

  float delta_position = (dp - (reflected_move + normal_dp)).Length();
  float current_delta_position = ball->CollisionDeltaPosition();

  if (delta_position < current_delta_position)
  {
    ball->PushCollisionPoint(collision_point);

    ball->SetCollisionPosition(final_position);
    ball->SetCollision(final_velocity, delta_position);
  }
  else
  {
    ball->SetCollision(final_velocity, current_delta_position);
  }

  float final_distance = n.Dot(final_position - A);
  if (Equals(final_distance, radius))
  {
    DECOMPOSE(final_velocity, n);
    vec3 N = -(ball->ConstantResultantForce()*n)*n;
    vec3 T = -tangential_final_velocity.Normalized() * friction * N.Length();

    ball->AddVariableForce(N + T);
  }
}

void Engine::ComputeBallBallCollision(Ball* ball1, Ball* ball2)
{
  if (ball1 == NULL || ball2 == NULL) {
    return;
  }

  const vec3& A = ball1->Position();
  const vec3& B = ball2->Position();

  // Calculate the movement vectors
  vec3 dp1 = ball1->NewPosition() - A;
  vec3 dp2 = ball2->NewPosition() - B;

  vec3 vec_AB = B - A;

  float distance = vec_AB.Length();
  float R = ball1->Radius() + ball2->Radius();

  if (distance < R && !Equals(distance, R))
  {
    vec3 u = vec_AB.Normalized();
    float k = 200.f * (ball1->Elasticity() + ball2->Elasticity());

    ball1->AddVariableForce( k * (distance - R) * u);
    ball2->AddVariableForce(-k * (distance - R) * u);
    return;
  }

  // Reduce dynamic-dynamic collision to dynamic-static
  vec3 rel_dp = dp1 - dp2;

  // If the length of the movement vector is less
  // than the distance between the objects,
  // then there is no way they can hit
  if (rel_dp.Length() + R < distance ) {
    return;
  }

  // Normalize the relative movement vector
  vec3 n = rel_dp.Normalized();

  // H is the point along 'A + vect(n)' which is the closest
  // to the point B (ball 2's center)
  float AH = n.Dot(vec_AB);
  float BH = Sqrt(distance*distance - AH*AH);

  // If the closest that A will get to B
  // is more than the sum of their radii,
  // there's no way they are going to collide
  if (BH > R) {
    return;
  }

  float collision_distance = R*R - BH*BH;

  // If there is no such right triangle with sides length of
  // sumRadii and sqrt(f), T will probably be less than 0.
  // Better to check now than perform a square root of a
  // negative number.
  if (collision_distance < 0.f) {
    return;
  }

  // Therefore the distance the circle has to travel along
  // the movement vector is D - sqrt(T)
  float max_distance = AH - Sqrt(collision_distance);

  // Get the magnitude of the movement vector
  float magnitude = rel_dp.Length();

  if (magnitude < max_distance) {
    return;
  }

  if (magnitude < 0.f || IsNull(magnitude)) {
    return;
  }

  if (max_distance < 0.f) {
    return;
  }

  float param = max_distance / magnitude;

  vec3 A_ = A + param * dp1;
  vec3 B_ = B + param * dp2;

  //
  // Collision response
  //

  float elasticity = Sqrt(ball1->Elasticity() * ball2->Elasticity());

  vec3 v1 = ball1->NewVelocity();
  vec3 v2 = ball2->NewVelocity();

  float m1 = ball1->Mass();
  float m2 = ball2->Mass();

  // First, find the normalized vector u from the center of
  // ball A to the center of ball B
  // This is the vector along which Momentum is exchanged
  vec3 u = (B_ - A_).Normalized();

  // Find the length of the component of each of the movement
  // vectors along n.
  // a1 = v1 . u
  // a2 = v2 . u
  float a1 = v1.Dot(u);
  float a2 = v2.Dot(u);

  // Using the optimized version,
  // optimized_p =  2(a1 - a2)
  //              -----------
  //                m1 + m2
  float optimized_p = 2.f * (a1 - a2) / (m1 + m2);

  // Calculate v1', the new movement vector of circle1
  // v1' = v1 - optimizedP * m2 * N
  vec3 v1_ = (v1 - (n * m1 * optimized_p)) * elasticity;

  // Calculate v1', the new movement vector of circle1
  // v2' = v2 + optimizedP * m1 * N
  vec3 v2_ = (v2 + (n * m2 * optimized_p)) * elasticity;

  float dp1_length = dp1.Length();
  float dp2_length = dp2.Length();

  ball1->SetCollision(v1_, param * dp1_length);
  ball2->SetCollision(v2_, param * dp2_length);

  ball1->SetCollisionPosition(A_ + v1_.Normalized()*(1.f-param)*dp1_length);
  ball2->SetCollisionPosition(B_ + v2_.Normalized()*(1.f-param)*dp2_length);
}
