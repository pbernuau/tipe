#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "globals.h"
#include "sceneitem.h"
#include "vec3.h"

//
// Triangle scene item
//
// One of the two collision shapes implemented in the engine.
// Triangles are not moveable items.
//
class Triangle : public SceneItem
{
public:
  Triangle(const vec3& A, const vec3& B, const vec3& C,
           float elasticity, float friction);

  // From SceneItem
  bool IsValid() const;
  AABBox BoundingBox() const;

  const vec3& Vertex(int i) const;
  const vec3& Normal() const;

  void Render(int debug_data) const;

protected:
  AABBox  _bounding_box;
  vec3    _vertices[3];
  vec3    _normal;
  DISALLOW_COPY_AND_ASSIGN(Triangle);
};

#endif // TRIANGLE_H
