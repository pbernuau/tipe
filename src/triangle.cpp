#include <qgl.h>

#include "drawbox.h"
#include "mymath.h"
#include "triangle.h"

#include <GL/gl.h>
#include <GL/glu.h>

Triangle::Triangle(const vec3& A, const vec3& B, const vec3& C,
                   float elasticity, float friction) :
  SceneItem(elasticity, friction),
  _bounding_box(A, B, C),
  _normal((B - A).Cross(C - B).Normalized())
{
  _vertices[0] = A;
  _vertices[1] = B;
  _vertices[2] = C;
}

bool Triangle::IsValid() const {
  return SceneItem::IsValid() && (_normal != vec3());
}

AABBox Triangle::BoundingBox() const {
  return _bounding_box;
}

const vec3& Triangle::Vertex(int i) const
{
  assert(i >= 0 && i <= 2);
  return _vertices[i];
}

const vec3& Triangle::Normal() const {
  return _normal;
}

void Triangle::Render(int debug_data) const
{
  UNUSED(debug_data);

  float color[4] = { 0.f };
  Color(&color[0], &color[1], &color[2]);

  if (debug_data & DEBUG_DATA_BOUNDING_BOX) {
    DrawBox(BoundingBox(), color[0], color[1], color[2]);
  }

  glBegin(GL_TRIANGLES);
  glColor3fv(color);
  for (int i = 0; i < 3; i++) {
    glVertex3fv(reinterpret_cast<const float*>(&_vertices[i]));
  }
  glEnd();
}

