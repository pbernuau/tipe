#include "state.h"

State operator*(float real, const State& state)
{
  State result(state);
  result.Velocity *= real;
  result.Position *= real;
  return result;
}

State operator*(const State& state, float real)
{
  return real * state;
}

State operator/(float real, const State& state)
{
  State result(state);
  result.Velocity /= real;
  result.Position /= real;
  return result;
}

State operator/(const State& state, float real)
{
  return real / state;
}

State operator+(const State& state1, const State& state2)
{
  State result(state1);
  result.Velocity += state2.Velocity;
  result.Position += state2.Position;
  return result;
}

