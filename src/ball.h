#ifndef BALL_H
#define BALL_H

#include <qlist.h>

#include "globals.h"
#include "sceneitem.h"
#include "state.h"
#include "vec3.h"

class Ball;

// ApplyForceFunc(ball, position, velocity) returns net force
typedef vec3 (*ApplyForceFunc)(Ball&, const vec3&, const vec3&);

//
// Ball scene item
//
// One of the two collision shapes implemented in the engine.
// Balls are moveable items.
//
class Ball : public SceneItem
{
public:
  Ball(float mass, float radius, float elasticity, float friction);
  ~Ball();

  // From SceneItem
  bool IsValid() const;
  AABBox BoundingBox() const;

  bool IsMoveable() const;

  const vec3& Position() const;
  void SetPosition(const vec3& position);

  const vec3& Velocity() const;
  void SetVelocity(const vec3& velocity);

  const vec3& Acceleration() const;
  void SetAcceleration(const vec3& acceleration);

  const vec3& NewPosition() const;
  void SetNewPosition(const vec3& position);

  const vec3& NewVelocity() const;
  void SetNewVelocity(const vec3& velocity);

  const vec3& NewAcceleration() const;
  void SetNewAcceleration(const vec3& acceleration);

  float Mass() const;
  float Radius() const;

  ReferenceCounted* Data() const;
  // Drops any previously stored data and grabs the new data if any.
  void SetData(ReferenceCounted* data);

  bool IsColliding() const;

  const vec3& CollisionPosition() const;
  void SetCollisionPosition(const vec3& position);

  const vec3& CollisionVelocity() const;
  float CollisionDeltaPosition() const;

  void SetCollision(const vec3& velocity, float delta);

  void ClearCollisionData();

  void PushCollisionPoint(const vec3& point);
  void PopCollisionPoint();

  ApplyForceFunc ApplyForceCallback() const;
  void SetApplyForceCallback(ApplyForceFunc callback);

  const vec3& ConstantResultantForce() const;
  void SetConstantResultantForce(const vec3& force);

  const vec3& VariableResultantForce() const;
  void AddVariableForce(const vec3& force);
  void ClearVariableResultantForce();

  void Render(int debug_data) const;

  // This operator is used in Runge-Kutta integration
  State operator()(float time, const State& state)
  {
    Q_UNUSED(time);

    vec3 force = _constant_resultant_force + _variable_resultant_force;
    if (_apply_force != NULL) {
     force += _apply_force(*this, state.Position, state.Velocity);
    }

    State result;
    result.Velocity = force / Mass();
    result.Position = state.Velocity;

    return result;
  }

private:
  // Basic data
  vec3      _position, _new_position,
            _velocity, _new_velocity,
            _acceleration, _new_acceleration;

  float     _mass,
            _radius;

  // User data
  ReferenceCounted* _data;

  // Collision data
  bool        _is_colliding;
  vec3        _collision_position,
              _collision_velocity;
  float       _collision_delta_position;
  QList<vec3> _collision_points;

  // Force data
  ApplyForceFunc  _apply_force;
  vec3            _constant_resultant_force;
  vec3            _variable_resultant_force;

  DISALLOW_COPY_AND_ASSIGN(Ball);
};

#endif // BALL_H
