#ifndef VEC3_INL_H
#define VEC3_INL_H

inline vec3::vec3() : x(0.f), y(0.f), z(0.f)
{
}

inline vec3::vec3(float x, float y, float z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}

inline vec3::vec3(const vec3& v)
{
  x = v.x;
  y = v.y;
  z = v.z;
}

inline vec3::vec3(const vec3& from, const vec3& to) {
  (*this) = to - from;
}

inline vec3& vec3::operator+=(const vec3& v)
{
  x += v.x;
  y += v.y;
  z += v.z;
  return *this;
}

inline vec3 vec3::operator+(const vec3& v) const {
  return vec3( x+v.x, y+v.y, z+v.z );
}

inline vec3&	vec3::operator-=(const vec3& v)
{
  x -= v.x;
  y -= v.y;
  z -= v.z;
  return *this;
}

inline vec3 vec3::operator-(const vec3& v) const {
  return vec3( x-v.x, y-v.y, z-v.z );
}

inline vec3 vec3::operator-() const {
  return vec3( -x, -y, -z );
}

inline vec3& vec3::operator*=(const float a)
{
  x *= a;
  y *= a;
  z *= a;
  return *this;
}

inline vec3 vec3::operator*(const float a) const {
  return vec3( x*a, y*a, z*a );
}

inline float vec3::operator*(const vec3& v) const {
  return this->Dot(v);
}

inline vec3& vec3::operator/=(const float a)
{
  x /= a;
  y /= a;
  z /= a;
  return *this;
}

inline vec3 vec3::operator/(const float a) const {
  return vec3( x/a, y/a, z/a );
}

inline vec3 operator*(const float a, const vec3& v) {
  return v*a;
}

#endif // VEC3_INL_H
