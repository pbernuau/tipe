#ifndef SCRIPTBOX_H
#define SCRIPTBOX_H

#include <QString>

class Engine;
class FreeFlyCamera;
class QScriptEngine;

//
// ScriptBox
//
// Exposes functions to JS context.
//
namespace ScriptBox
{
  void Init(Engine* engine);
  void Release();

  void SetCamera(FreeFlyCamera* camera);

  void Tick(int time_step, bool idle);

  void Eval(const QString& filename);
}

#endif // SCRIPTBOX_H
