#include <qfile.h>

#include <qscriptcontext.h>
#include <qscriptengine.h>
#include <qscriptvalue.h>

#include <QtDebug>

#include "ball.h"
#include "engine.h"
#include "freeflycamera.h"
#include "referencecounted.h"
#include "scriptbox.h"
#include "triangle.h"

static Engine*        g_physics_engine = NULL;
static FreeFlyCamera* g_camera = NULL;
static QScriptEngine* g_script_engine = NULL;

struct BallData : public ReferenceCounted
{
  bool          use_rungekutta;
  QScriptValue  callback;
};

static void HandleUncaughtException()
{
  if (g_script_engine->hasUncaughtException())
  {
    qWarning("%s (line %i)", static_cast<const char*>(
              g_script_engine->uncaughtException().toString().toAscii()),
              g_script_engine->uncaughtExceptionLineNumber());
  }
  g_script_engine->clearExceptions();
}

QScriptValue CreateBall(QScriptContext* context, QScriptEngine* engine);
QScriptValue CreateTriangle(QScriptContext* context, QScriptEngine* engine);
QScriptValue Print(QScriptContext* context, QScriptEngine* engine);
QScriptValue ReadFile(QScriptContext* context, QScriptEngine* engine);
QScriptValue Include(QScriptContext* context, QScriptEngine* engine);
QScriptValue DeleteItems(QScriptContext* context, QScriptEngine* engine);

#define EXPOSE_FN(global, name) \
  global.setProperty(#name, g_script_engine->newFunction(name));

// Callback function that calls the provided js function
// (that is stored inside BallData)
static vec3 ApplyForce(Ball& ball, const vec3& pos, const vec3& vel)
{
  vec3 force;
  BallData& data = dynamic_cast<BallData&>(*ball.Data());

  QScriptValue callback = data.callback;

  if (callback.isFunction())
  {
    QScriptValue  obj = g_script_engine->newObject(),
                  posObj = g_script_engine->newArray(3),
                  velObj = g_script_engine->newArray(3);

    posObj.setProperty(0, pos.x);
    posObj.setProperty(1, pos.y);
    posObj.setProperty(2, pos.z);

    velObj.setProperty(0, vel.x);
    velObj.setProperty(1, vel.y);
    velObj.setProperty(2, vel.z);

    obj.setProperty("position", posObj);
    obj.setProperty("velocity", velObj);
    obj.setProperty("mass", ball.Mass());
    obj.setProperty("radius", ball.Radius());
    obj.setProperty("name", ball.Name());

    QScriptValue return_value = callback.call(obj);
    force.x = return_value.property(0).toNumber();
    force.y = return_value.property(1).toNumber();
    force.z = return_value.property(2).toNumber();
  }

  return force;
}

void ScriptBox::Init(Engine *engine)
{
  assert(g_physics_engine == NULL && g_script_engine == NULL);

  g_physics_engine = engine;
  g_script_engine = new QScriptEngine;

  QScriptValue global = g_script_engine->globalObject();

  EXPOSE_FN(global, CreateBall);
  EXPOSE_FN(global, CreateTriangle);
  EXPOSE_FN(global, Print);
  EXPOSE_FN(global, ReadFile);
  EXPOSE_FN(global, Include);
  EXPOSE_FN(global, DeleteItems);
}

void ScriptBox::Release()
{
  delete g_script_engine;

  g_physics_engine = NULL;
  g_script_engine = NULL;
}

void ScriptBox::SetCamera(FreeFlyCamera* camera)
{
  g_camera = camera;
}

void ScriptBox::Tick(int time_step, bool idle)
{
  QScriptValue this_ref = g_script_engine->newObject();
  this_ref.setProperty("timestep", time_step);
  this_ref.setProperty("idle", idle);

  g_script_engine->globalObject().property("Tick").call(this_ref);
  HandleUncaughtException();
}

void ScriptBox::Eval(const QString& filename)
{
  QFile file(filename);
  if (file.open(QFile::ReadOnly | QIODevice::Text))
  {
    g_script_engine->currentContext()->setThisObject(
        g_script_engine->globalObject());
    g_script_engine->evaluate(file.readAll(), filename).toString();

    HandleUncaughtException();
  }
  else
  {
    qWarning(file.errorString().toAscii());
  }
}

//bool function CreateBall()
QScriptValue CreateBall(QScriptContext* context, QScriptEngine*)
{
  QScriptValue arguments = context->argument(0);

  vec3    position,
          velocity,
          initial_force,
          color(0.8f, 0.8f, 0.8f);
  float   mass = 1.f,
          radius = 1.f,
          elasticity = 0.9f,
          friction = 0.9f;
  int     name = 0;
  vec3    const_force = vec3(0.f, 0.f, -9.81f);
  bool    use_rungekutta = true;

  QScriptValue apply_force_function;

  position.x = arguments.property("position").property(0).toNumber();
  position.y = arguments.property("position").property(1).toNumber();
  position.z = arguments.property("position").property(2).toNumber();

  velocity.x = arguments.property("velocity").property(0).toNumber();
  velocity.y = arguments.property("velocity").property(1).toNumber();
  velocity.z = arguments.property("velocity").property(2).toNumber();

  initial_force.x = arguments.property("initialforce").property(0).toNumber();
  initial_force.y = arguments.property("initialforce").property(1).toNumber();
  initial_force.z = arguments.property("initialforce").property(2).toNumber();

  color.x = arguments.property("color").property(0).toNumber();
  color.y = arguments.property("color").property(1).toNumber();
  color.z = arguments.property("color").property(2).toNumber();

  if (arguments.property("mass").isValid())
  {
    mass = arguments.property("mass").toNumber();
    const_force *= mass;
  }

  if (arguments.property("radius").isValid()) {
    radius = arguments.property("radius").toNumber();
  }

  if (arguments.property("elasticity").isValid()) {
    elasticity = arguments.property("elasticity").toNumber();
  }

  if (arguments.property("friction").isValid()) {
    friction = arguments.property("friction").toNumber();
  }

  if (arguments.property("name").isValid()) {
    name = arguments.property("name").toInteger();
  }

  if (arguments.property("constantforce").isValid())
  {
    const_force.x = arguments.property("constantforce").property(0).toNumber();
    const_force.y = arguments.property("constantforce").property(1).toNumber();
    const_force.z = arguments.property("constantforce").property(2).toNumber();
  }

  apply_force_function = arguments.property("callback");

  Ball* ball = new Ball(mass, radius, elasticity, friction);
  ball->SetPosition(position);
  ball->SetNewPosition(position);
  ball->SetVelocity(velocity);
  ball->SetColor(color.x, color.y, color.z);
  ball->SetName(name);
  ball->SetConstantResultantForce(const_force);
  ball->AddVariableForce(initial_force);

  if (arguments.property("rungekutta").isValid()) {
    use_rungekutta = arguments.property("rungekutta").toBool();
  }
  if (!g_physics_engine->AddBall(ball))
  {
    ball->Drop();
    return false;
  }

  // The ball is from now on held by the physics engine.

  BallData* data = new BallData;
  data->use_rungekutta = use_rungekutta;
  data->callback = apply_force_function;

  ball->SetData(data);
  ball->SetApplyForceCallback(ApplyForce);

  // Leave the ownership to the physics engine.
  ball->Drop();

  return true;
}

// bool function CreateTriangle
QScriptValue CreateTriangle(QScriptContext* context, QScriptEngine*)
{
  QScriptValue arguments = context->argument(0);

  vec3    vertices[3],
          color(0.8f, 0.8f, 0.8f);
  float   elasticity = 0.9f,
          friction = 0.9f;
  int     name = 0;

  // Vertices
  for (int i = 0; i < 3; i++)
  {
    QScriptValue val = arguments.property("vertices").property(i);
    vertices[i].x = val.property(0).toNumber();
    vertices[i].y = val.property(1).toNumber();
    vertices[i].z = val.property(2).toNumber();
  }

  color.x = arguments.property("color").property(0).toNumber();
  color.y = arguments.property("color").property(1).toNumber();
  color.z = arguments.property("color").property(2).toNumber();

  if (arguments.property("elasticity").isValid()) {
    elasticity = arguments.property("elasticity").toNumber();
  }

  if (arguments.property("friction").isValid()) {
    friction = arguments.property("friction").toNumber();
  }

  if (arguments.property("name").isValid()) {
    name = arguments.property("name").toInteger();
  }

  Triangle* triangle = new Triangle(vertices[0], vertices[1], vertices[2],
                                    elasticity, friction);
  if (!g_physics_engine->AddTriangle(triangle))
  {
    triangle->Drop();
    return false;
  }

  // From now on, the triangle is held by the physics engine.

  triangle->SetName(name);
  triangle->SetColor(color.x, color.y, color.z);

  if (arguments.property("immaterial").isValid()) {
    triangle->SetImmaterial(arguments.property("immaterial").toBool());
  }

  // Leave the ownership to the physics engine.
  triangle->Drop();

  return true;
}

// void function Print(String text)
QScriptValue Print(QScriptContext* context, QScriptEngine*)
{
  for (int i = 0; i < context->argumentCount(); i++) {
    qDebug(context->argument(i).toString().toAscii());
  }

  return QScriptValue();
}

// String function ReadFile(String filename)
QScriptValue ReadFile(QScriptContext* context, QScriptEngine*)
{
  QFile file(context->argument(0).toString());
  if (file.open(QFile::ReadOnly | QIODevice::Text))
  {
    return QString(file.readAll());
  }

  return context->throwError(file.errorString()).toString();
}

//void ScriptBox::Include(const QString & filename)
QScriptValue Include(QScriptContext* context, QScriptEngine* engine)
{
  QString filename(context->argument(0).toString());
  QFile file(filename);
  if (file.open(QFile::ReadOnly | QIODevice::Text))
  {
    engine->currentContext()->setThisObject(engine->globalObject());
    engine->evaluate(file.readAll(), filename).toString();

    HandleUncaughtException();
  }
  else
  {
    qWarning(file.errorString().toAscii());
  }

  return engine->nullValue();
}

// int function DeleteItems(int name)
QScriptValue DeleteItems(QScriptContext* context, QScriptEngine*)
{
  return g_physics_engine->DeleteItems(context->argument(0).toInteger());
}
