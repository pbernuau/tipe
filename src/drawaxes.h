#ifndef DRAWAXES_H
#define DRAWAXES_H

#include "vec3.h"

//
// DrawAxes
//
// It is up to the caller to disable lighting if necessary.
// For each axis, a 'length'-unit-long line is rendered.
// Colors are:
//   x - red
//   y - green
//   z - blue
void DrawAxes(float length = 10.f, const vec3 origin = vec3());

#endif // DRAWAXES_H
