#ifndef OCTREE_H
#define OCTREE_H

#include <qlist.h>
#include <qpair.h>
#include <qset.h>

#include "aabbox.h"
#include "globals.h"
#include "sceneitem.h"

//
// SceneNode
//
// Recursive structure used to build an Octree.
// Either it holds SceneNode children or static/dynamic items.
//
class SceneNode
{
public:
  int AddItem(SceneItem* item);
  void RemoveItem(SceneItem* item);

  int UpdateStructure();

  void GetCollisions(QSet< QPair<SceneItem*, SceneItem*> >& collisions) const;
  void GetBoundingItems(const AABBox& box, QSet<SceneItem*>& items) const;

  typedef void (*ApplyFn)(const SceneNode*);
  enum ApplyMode
  {
    AM_LEAVES_ONLY,
    AM_ROOT_FIRST,
    AM_LEAVES_FIRST
  };

  void Apply(ApplyFn fn, ApplyMode mode) const;

  void Reset();
  void UnmarkItems();

  const AABBox& Box() const;
  // The node is a leaf if it has no children.
  // (ie the node is not subdivided)
  bool IsLeaf() const;

  bool HasDynamicItems() const;

  void GetAllItems(QSet<SceneItem*>& static_items,
                   QSet<SceneItem*>& dynamic_items) const;

protected:
  SceneNode(SceneNode* parent, const AABBox& box);
  ~SceneNode();

  bool NeedsSubdivision() const;

  void TriggerBubbling();
  void BubbleUp(SceneItem* item);
  void BubbleDown();

  void GrabItem(SceneItem* item);
  void DropItem(SceneItem* item);

private:
  AABBox              _box;
  SceneNode*          _parent;
  QSet<SceneItem*>    _static_items;
  QSet<SceneItem*>    _dynamic_items;
  QList<SceneNode*>   _children;
};

//
// Octree
//
// Root SceneNode.
//
class Octree : public SceneNode
{
public:
  Octree() :
    SceneNode(NULL, AABBox(kOctreeSize))
  {
  }

  void UpdateMoveableItems()
  {
    UnmarkItems();
    TriggerBubbling();
    BubbleDown();
  }
};

#endif // OCTREE_H
