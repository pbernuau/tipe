#ifndef GLOBALS_H
#define GLOBALS_H

#include <cassert>

// A macro to disallow copy constructor and operator=
// This should be used at the end of the private section
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)

#ifndef NDEBUG
# define CHECK_POINTER(pointer) assert(pointer != NULL);
#else
# define CHECK_POINTER(pointer)
#endif

#define UNUSED(x) (void)x

enum DebugData
{
  DEBUG_DATA_NONE             = 0,
  DEBUG_DATA_NET_FORCE        = 1,
  DEBUG_DATA_VELOCITY         = 2,
  DEBUG_DATA_BOUNDING_BOX     = 4,
  DEBUG_DATA_COLLISION_POINTS = 8,
  DEBUG_DATA_OCTREE           = 16,
  DEBUG_DATA_NO_DRAW          = 32
};

const int   kMinItemsPerNode  = 4;
// The two following constants define the rules to determine
// whether to split a node or not.
// The node need splitting if :
//  - it has at least 'kMaxItemsPerNode' items
//  - and is it smaller than 'kMinNodeSize'
const int   kMaxItemsPerNode  = 10;
const float kMinNodeSize      = 10.f;

// Size of the world's bounding box.
// Everything outside that box is discarded.
const float kOctreeSize       = 1000.f;


const float kLineWidth = 1.f;
const float kPointSize = 1.f;

#endif // GLOBALS_H
