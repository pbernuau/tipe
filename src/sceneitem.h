#ifndef SCENEITEM_H
#define SCENEITEM_H

#include "aabbox.h"
#include "globals.h"
#include "referencecounted.h"

//
// SceneItem
//
// Abstract class for collision shapes implemented in the physics engine.
// Ball and Triangle inherit from this class.
//
class SceneItem : public ReferenceCounted
{
public:
  SceneItem(float elasticity, float friction);
  virtual ~SceneItem() {}

  // Each subclass reimplementing IsValid should also call the base method.
  virtual bool IsValid() const;
  virtual bool IsMoveable() const;

  void Mark();
  void Unmark();
  bool IsMarked() const;

  float Elasticity() const;
  void SetElasticity(float elasticity);

  float Friction() const;
  void SetFriction(float friction);

  int Name() const;
  void SetName(int name);

  void Color(float* red, float* green, float* blue) const;
  void SetColor(float red, float green, float blue);

  virtual AABBox BoundingBox() const = 0;

  virtual void Render(int debug_data) const;

  bool Immaterial() const;
  void SetImmaterial(bool immaterial);

private:
  float   _elasticity;
  float   _friction;
  bool    _immaterial;
  int     _name;
  float   _red;
  float   _green;
  float   _blue;

  bool    _marked;

  DISALLOW_COPY_AND_ASSIGN(SceneItem);
};

#endif // SCENEITEM_H
