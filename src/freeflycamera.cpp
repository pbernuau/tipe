#define _USE_MATH_DEFINES
#include <cmath>

#include <qgl.h>

#include "freeflycamera.h"

#include <GL/gl.h>
#include <GL/glu.h>

FreeFlyCamera::FreeFlyCamera(const vec3& position)
{
  _speed = 0.1f;
  _sensivity = 0.2f;

  _position = position;
  _phi = -45.f;
  _theta = 180.f;

  VectorsFromAngles();
}

FreeFlyCamera::~FreeFlyCamera()
{
}

void FreeFlyCamera::Move(int dt)
{
  _position += _forward * _speed * dt;
  _target = _position + _forward;
}

void FreeFlyCamera::Strate(int dt)
{
  _position += _left * _speed * dt;
  _target = _position + _forward;
}

void FreeFlyCamera::Rotate(float dx, float dy)
{
  _theta -= dx * _sensivity;
  _phi -= dy * _sensivity;
  VectorsFromAngles();
}

void FreeFlyCamera::SetSpeed(float speed)
{
  _speed = speed;
}

void FreeFlyCamera::SetSensivity(float sensivity)
{
  _sensivity = sensivity;
}

void FreeFlyCamera::SetPosition(const vec3& position)
{
  _position = position;
  _target = _position + _forward;
}

void FreeFlyCamera::SetTarget(const vec3& target)
{
  _target = target;
}

const vec3& FreeFlyCamera::Position() const {
  return _position;
}

const vec3& FreeFlyCamera::Target() const {
  return _target;
}

void FreeFlyCamera::VectorsFromAngles()
{
  static const vec3 up(0.f, 0.f, 1.f);
  if (_phi > 89.f) {
    _phi = 89.f;
  }
  else if (_phi < -89.f) {
    _phi = -89.f;
  }

  float r_temp = cos(_phi * M_PI / 180.f);
  _forward.z = sin(_phi * M_PI / 180.f);
  _forward.x = r_temp*cos(_theta * M_PI / 180.f);
  _forward.y = r_temp*sin(_theta * M_PI / 180.f);

  _left = up.Cross(_forward);
  _left.Normalize();

  _target = _position + _forward;
}

void FreeFlyCamera::Look()
{
  gluLookAt(_position.x, _position.y, _position.z,
            _target.x, _target.y, _target.z,
            0.f, 0.f, 1.f);

  GLfloat light_position[4] =
  {
    _position.x,
    _position.y,
    _position.z,
    1.0f
  };

  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
}
