#ifndef AABBOX_H
#define AABBOX_H

#include "vec3.h"

//
// AABBox (Axis-Aligned Bounding Box)
//
// All scene items are included in a bounding box.
// Bounding boxes are used to roughly determine collisions.
//
class AABBox
{
public:
  AABBox(const AABBox& box);
  explicit AABBox(const vec3& point);
  explicit AABBox(float size);
  AABBox(const vec3& point1, const vec3& point2);
  AABBox(const vec3& point1, const vec3& point2, const vec3& point3);

  void Assign(const AABBox& box);
  void AddInternalPoint(const vec3& pt);
  void AddInternalPoint(float x, float y, float z);
  void AddInternalBox(const AABBox& box);

  void Translate(const vec3& translation);

  void Reset(const vec3& point);
  void Repair();

  bool Equals(const AABBox& other) const;

  vec3 GetCenter() const;
  vec3 GetExtent() const;
  void GetVertices(vec3* vertices) const;

  bool IsEmpty() const;

  bool IsPointInside(const vec3& point) const;
  bool IsPointTotalInside(const vec3& point) const;
  bool IsFullInside(const AABBox& box) const;
  bool IntersectsWithBox(const AABBox& box) const;

  void Shrink(float value);

private:
  vec3	_min_edge;
  vec3	_max_edge;
};

#endif // AABBOX_H
