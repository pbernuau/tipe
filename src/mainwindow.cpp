#include "qaction.h"
#include "qdockwidget.h"
#include "qkeysequence.h"
#include "qmenubar.h"
#include "qplaintextedit.h"

#include "globals.h"
#include "glwidget.h"
#include "mainwindow.h"

QPlainTextEdit* g_console = NULL;

void Print(const char* text)
{
  static QString buffer;

  if (g_console == NULL)
  {
    buffer.append(QString(text) + "\n");
  }
  else
  {
    if (!buffer.isEmpty()) {
      g_console->appendPlainText(buffer);
      buffer.clear();
    }

    g_console->appendPlainText(text);
  }
}

void MsgHandler(QtMsgType, const char* text)
{
  Print(text);
}

MainWindow::MainWindow() : QMainWindow()
{
  qInstallMsgHandler(MsgHandler);

  _central_widget = new GLWidget(this);
  setCentralWidget(_central_widget);

  QDockWidget* dock = new QDockWidget(tr("Moniteur"), this);
  QPlainTextEdit* edit = new QPlainTextEdit(dock);
  dock->setWidget(edit);
  addDockWidget(Qt::RightDockWidgetArea, dock);

  QAction* animate = new QAction(tr("Jouer l'animation"), this);
  animate->setShortcut(QKeySequence(Qt::Key_F4));
  animate->setCheckable(true);
  animate->setChecked(false);
  connect(animate, SIGNAL(triggered(bool)),
          _central_widget, SLOT(SetAnimate(bool)));

  QAction* trace = new QAction(tr("Afficher la trace"), this);
  trace->setShortcut(QKeySequence(Qt::Key_F2));
  trace->setCheckable(true);
  connect(trace, SIGNAL(triggered(bool)),
          _central_widget, SLOT(SetTraceEnabled(bool)));

  QAction* show_net_force = new QAction(tr("Afficher la r�sultante"), this);
  show_net_force->setCheckable(true);
  connect(show_net_force, SIGNAL(triggered(bool)),
          _central_widget, SLOT(ShowNetForce(bool)));

  QAction* show_velocity = new QAction(tr("Afficher la vitesse"), this);
  show_velocity->setCheckable(true);
  connect(show_velocity, SIGNAL(triggered(bool)),
          _central_widget, SLOT(ShowVelocity(bool)));

  QAction* show_bounding_box = new QAction(tr("Afficher les bo�tes"), this);
  show_bounding_box->setCheckable(true);
  connect(show_bounding_box, SIGNAL(triggered(bool)),
          _central_widget, SLOT(ShowBoundingBox(bool)));

  QAction* show_collision_points =
      new QAction(tr("Afficher les points de collisions"), this);
  show_collision_points->setCheckable(true);
  connect(show_collision_points, SIGNAL(triggered(bool)),
          _central_widget, SLOT(ShowCollisionPoints(bool)));

  QAction* show_octree = new QAction(tr("Afficher l'arbre"), this);
  show_octree->setCheckable(true);
  connect(show_octree, SIGNAL(triggered(bool)),
          _central_widget, SLOT(ShowOctree(bool)));

  QAction* no_draw = new QAction(tr("D�sactiver l'affichage"), this);
  no_draw->setCheckable(true);
  connect(no_draw, SIGNAL(triggered(bool)),
          _central_widget, SLOT(NoDraw(bool)));

  QMenu* scene_menu = menuBar()->addMenu("Sc�ne");
  scene_menu->addAction(animate);
  scene_menu->addAction(trace);

  QMenu* debug_menu = menuBar()->addMenu("Debug");
  debug_menu->addAction(show_net_force);
  debug_menu->addAction(show_velocity);
  debug_menu->addAction(show_bounding_box);
  debug_menu->addAction(show_collision_points);
  debug_menu->addAction(show_octree);
  debug_menu->addAction(no_draw);

  g_console = edit;
  Print("Z, S, Q, D : se d�placer");
  Print("R : r�initialiser");
  Print("O : ouvrir une sc�ne");
  Print("F2 : afficher la trace");
  Print("F4 : jouer l'animation");
  Print("ESPACE : jouer une �tape");
  Print("F12 : prendre une capture d'�cran");
  Print(NULL);
}

MainWindow::~MainWindow()
{
  qInstallMsgHandler(0);
}
