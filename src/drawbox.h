#ifndef DRAWBOX_H
#define DRAWBOX_H

class AABBox;

//
// DrawBox
//
// Renders a wireframe box using rgb-defined color.
//
void DrawBox(const AABBox& box, float r, float g, float b);

#endif // DRAWBOX_H
