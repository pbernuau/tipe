#include <cmath>

#include "mymath.h"

const float kEpsilon = 1e-2;

bool Equals(float x, float y)
{
  return ((x-y) < kEpsilon) && ((x-y) > -kEpsilon);
}

bool IsNull(float x)
{
  return (x < kEpsilon) && (x > -kEpsilon);
}

float Sqrt(float number)
{
  long i;
  float x, y;
  const float f = 1.5F;

  x = number * 0.5F;
  y  = number;
  i  = * ( long * ) &y;
  i  = 0x5f3759df - ( i >> 1 );
  y  = * ( float * ) &i;
  y  = y * ( f - ( x * y * y ) );
  y  = y * ( f - ( x * y * y ) );
  return number * y;
}

float Abs(float x)
{
  return fabs(x);
}
