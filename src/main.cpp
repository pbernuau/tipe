#include <qapplication.h>
#include <qdatetime.h>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
  QApplication application(argc, argv);
  qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

  MainWindow window;
  window.show();
  return application.exec();
}
