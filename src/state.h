#ifndef STATE_H
#define STATE_H

#include "vec3.h"

struct State
{
  vec3  Velocity;
  vec3  Position;
};

State operator*(float real, const State& state);
State operator*(const State& state, float real);
State operator/(float real, const State& state);
State operator/(const State& state, float real);
State operator+(const State& state1, const State& state2);

#endif // STATE_H
