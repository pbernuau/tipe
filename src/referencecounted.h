#ifndef REFERENCECOUNTED_H
#define REFERENCECOUNTED_H

#include "globals.h"

//
// ReferenceCounted
//
// This class is meant to be inherited from.
// It holds a reference counter that counts how many times the object
// is used in different places.
// When the object is no longer used it will be deleted automatically.
//
class ReferenceCounted
{
public:
  ReferenceCounted() : _reference_count(1) {}
  virtual ~ReferenceCounted() {}

  void Grab() const {
    const_cast<ReferenceCounted*>(this)->_reference_count++;
  }

  void Drop() const
  {
    assert(_reference_count > 0);

    const_cast<ReferenceCounted*>(this)->_reference_count--;
    if (_reference_count == 0) {
      delete this;
    }
  }

  int ReferenceCount() const {
    return _reference_count;
  }

private:
  int _reference_count;

  DISALLOW_COPY_AND_ASSIGN(ReferenceCounted);
};

#endif // REFERENCECOUTED_H
