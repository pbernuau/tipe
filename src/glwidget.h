#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <qcolor.h>
#include <qdatetime.h>
#include <qevent.h>
#include <qgl.h>
#include <qpoint.h>
#include <qsize.h>

#include "engine.h"
#include "globals.h"
#include "freeflycamera.h"

//
// GLWidget
//
// The Qt Widget that initializes OpenGL functions
// and that processes GUI events.
//
class GLWidget : public QGLWidget
{
  Q_OBJECT

public:
  GLWidget(QWidget* parent = 0);
  ~GLWidget();

  QSize minimumSizeHint() const;
  QSize sizeHint() const;

public slots:
  void SetAnimate(bool animate);
  void SetTraceEnabled(bool enabled);

  void ShowNetForce(bool show);
  void ShowVelocity(bool show);
  void ShowBoundingBox(bool show);
  void ShowCollisionPoints(bool show);
  void ShowOctree(bool show);
  void NoDraw(bool no_draw);

  void ScreenShot(const QString& target_filename);

protected:
  void initializeGL();
  void paintGL();
  void resizeGL(int width, int height);

  void mouseMoveEvent(QMouseEvent* event);
  void mousePressEvent(QMouseEvent* event);
  void keyPressEvent(QKeyEvent* event);
  void keyReleaseEvent(QKeyEvent* event);

private:
  QTime         _clock;
  bool	        _animate;
  bool          _trace_enabled;
  int           _debug_data_options;
  bool          _do_one_step;
  Engine        _engine;

  FreeFlyCamera _camera;
  QPoint			  _last_mouse_position;
  int				    _move;
  int				    _strate;
};

#endif // GLWIDGET_H
