#include <qgl.h>

#include <qapplication.h>
#include <qdesktopwidget.h>
#include <qfiledialog.h>
#include <qimage.h>
#include <qtimer.h>

#include <qpainter.h>

#include "drawaxes.h"
#include "globals.h"
#include "glwidget.h"
#include "mymath.h"
#include "scriptbox.h"

#include <GL/gl.h>
#include <GL/glu.h>

GLWidget::GLWidget(QWidget* parent) :
  QGLWidget(QGLFormat(QGL::SampleBuffers), parent),
  _animate(false),
  _trace_enabled(false),
  _debug_data_options(DEBUG_DATA_NONE),
  _do_one_step(false),
  _camera(vec3(200.f, 0.f, 200.f)),
  _move(0),
  _strate(0)
{
  setFocusPolicy(Qt::ClickFocus);

  QTimer* timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(update()));
  timer->start(0);

  /*connect(&_script_box, SIGNAL(ScreenShot(const QString&)),
          this, SLOT(ScreenShot(const QString&)));*/

  ScriptBox::SetCamera(&_camera);
  ScriptBox::Init(&_engine);
  //ScriptBox::Eval("scene.js");
}

GLWidget::~GLWidget()
{
  ScriptBox::Release();
}

QSize GLWidget::minimumSizeHint() const
{
  return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
  return QSize(800, 600);
}

void GLWidget::SetAnimate(bool animate)
{
  _animate = animate;
}

void GLWidget::SetTraceEnabled(bool enabled)
{
  _trace_enabled = enabled;
}

void GLWidget::ShowNetForce(bool show)
{
  if (show) {
    _debug_data_options |= DEBUG_DATA_NET_FORCE;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_NET_FORCE;
  }
}

void GLWidget::ShowVelocity(bool show)
{
  if (show) {
    _debug_data_options |= DEBUG_DATA_VELOCITY;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_VELOCITY;
  }
}

void GLWidget::ShowBoundingBox(bool show)
{
  if (show) {
    _debug_data_options |= DEBUG_DATA_BOUNDING_BOX;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_BOUNDING_BOX;
  }
}

void GLWidget::ShowCollisionPoints(bool show)
{
  if (show) {
    _debug_data_options |= DEBUG_DATA_COLLISION_POINTS;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_COLLISION_POINTS;
  }
}

void GLWidget::ShowOctree(bool show)
{
  if (show) {
    _debug_data_options |= DEBUG_DATA_OCTREE;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_OCTREE;
  }
}

void GLWidget::NoDraw(bool no_draw)
{
  if (no_draw) {
    _debug_data_options |= DEBUG_DATA_NO_DRAW;
  }
  else {
    _debug_data_options &= ~DEBUG_DATA_NO_DRAW;
  }
}

void GLWidget::ScreenShot(const QString& target_filename)
{
  QTime start_time(_clock);
  grabFrameBuffer().save(QString("./screenshots/")+target_filename);
  _clock = start_time;
}

void GLWidget::initializeGL()
{
  qglClearColor(QColor::fromRgbF(1.0, 1.0,1.0, 1.0));

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glEnable(GL_LIGHT0);

  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);

  glShadeModel(GL_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT,            GL_NICEST);
  glHint(GL_LINE_SMOOTH_HINT,             GL_NICEST);
  glHint(GL_POLYGON_SMOOTH_HINT,          GL_NICEST);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT,  GL_NICEST);

  glLineWidth(kLineWidth);
  glPointSize(kPointSize);
  _clock.restart();
}

void GLWidget::paintGL()
{
  int dt = _clock.restart();

  if (_do_one_step) {
    dt = 10;
  }

  float time_step = static_cast<float>(dt) / 1000.f;
  bool force_clear = false;

  if (_move != 0)
  {
    _camera.Move(dt * _move);
    force_clear = true;
  }

  if (_strate != 0)
  {
    _camera.Strate(dt * _strate);
    force_clear = true;
  }

  if (_animate || _do_one_step) {
    _engine.Animate(time_step);
  }

  ScriptBox::Tick(dt, !_animate);

  if (!_trace_enabled || force_clear) {
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
  }

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  _camera.Look();

  DrawAxes(10.f);

  _engine.Render(_debug_data_options);

  static int frames = 0;
  static int fps = 0;
  static int ms = 0;
  static int t = 0;
  ms += dt;

  if (_animate || _do_one_step) {
    t += dt;
  }
  frames++;

  if (ms > 1000)
  {
    ms -= 1000;
    fps = frames;
    frames = 0;
  }

  glColor3f(0.f, 0.f, 0.f);
  renderText(0, 10,
             QString("%1 fps // t = %3").arg(fps).arg(t),
             QFont("Consolas", 10, QFont::Bold));

  _do_one_step = false;
}

void GLWidget::resizeGL(int width, int height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  double aspect = static_cast<double>(width) / static_cast<double>(height);

  gluPerspective(70.0, aspect, 1.0, 10000.0);

  if (_trace_enabled) {
    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
  }
}

void GLWidget::mouseMoveEvent(QMouseEvent* event)
{
  if (!event->buttons().testFlag(Qt::LeftButton))
    return;

  event->accept();

  QPoint dl = event->pos() - _last_mouse_position;
  _camera.Rotate(dl.x(), dl.y());
  _last_mouse_position = event->pos();

  if (_trace_enabled) {
    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
  }
}

void GLWidget::mousePressEvent(QMouseEvent* event)
{
  if (event->button() != Qt::LeftButton)
    return;

  event->accept();
  _last_mouse_position = event->pos();
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
  switch (event->key())
  {
  case Qt::Key_Z:
    _move = 1;
    break;

  case Qt::Key_S:
    _move = -1;
    break;

  case Qt::Key_Q:
    _strate = 1;
    break;

  case Qt::Key_D:
    _strate = -1;
    break;

  case Qt::Key_R:
    _engine.Reset();
    break;

  case Qt::Key_O:
    {
      QString fileName = QFileDialog::getOpenFileName(
        this, QString(), "./scripts/", "Javascript File (*.js)");
      if (!fileName.isEmpty()) {
        ScriptBox::Eval(fileName);
      }
    }
    break;

  case Qt::Key_F12:
    ScreenShot(QString("%1.png").arg(
        QTime::currentTime().toString("hh_mm_ss_zzz")));
    break;

  case Qt::Key_Space:
    _do_one_step = true;
    break;
  }
}

void GLWidget::keyReleaseEvent(QKeyEvent* event)
{
  if (event->key() == Qt::Key_Z && _move == 1) {
    _move = 0;
  }
  else if (event->key() == Qt::Key_S && _move == -1) {
    _move = 0;
  }
  else if (event->key() == Qt::Key_Q && _strate == 1) {
    _strate = 0;
  }
  else if (event->key() == Qt::Key_D && _strate == -1) {
    _strate = 0;
  }
}
