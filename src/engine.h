#ifndef ENGINE_H
#define ENGINE_H

#include <qmap.h>
#include <qlist.h>

#include "globals.h"
#include "octree.h"
#include "vec3.h"

class Ball;
class SceneItem;
class Triangle;

//
// Engine
//
// The core component that computes physics.
//
class Engine
{
public:
  Engine();
  ~Engine();

  bool AddBall(Ball* ball);
  bool AddTriangle(Triangle* triangle);

  // Deletes items by name.
  // Returns the number of items deleted.
  int DeleteItems(int name);
  int DeleteItem(SceneItem* item);

  void Reset();

  void Animate(float dt);
  void Render(int debug_data);

protected:
  void ApplyForces(float dt);

  void ComputeBallBallCollision(Ball* ball1, Ball* ball2);
  void ComputeBallTriangleCollision(Ball* ball, Triangle* triangle);

private:
  QList<SceneItem*>   _items;
  QList<Ball*>        _balls;
  QList<Triangle*>    _triangles;
  Octree              _octree;
  int                 _previous_render_mode;
  bool                _reset_display_list;

  DISALLOW_COPY_AND_ASSIGN(Engine);
};

#endif // ENGINE_H
