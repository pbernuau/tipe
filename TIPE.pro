
QT       += core gui opengl script

TARGET = TIPE
TEMPLATE = app

RC_FILE = TIPE.rc

HEADERS += \
    src/vec3-inl.h \
    src/vec3.h \
    src/triangle.h \
    src/scriptbox.h \
    src/sceneitem.h \
    src/rungekutta.h \
    src/referencecounted.h \
    src/mymath.h \
    src/mainwindow.h \
    src/glwidget.h \
    src/globals.h \
    src/freeflycamera.h \
    src/engine.h \
    src/drawbox.h \
    src/drawaxes.h \
    src/ball.h \
    src/aabbox.h \
    src/octree.h \
    src/state.h

SOURCES += \
    src/vec3.cpp \
    src/triangle.cpp \
    src/scriptbox.cpp \
    src/sceneitem.cpp \
    src/mymath.cpp \
    src/mainwindow.cpp \
    src/main.cpp \
    src/glwidget.cpp \
    src/freeflycamera.cpp \
    src/engine.cpp \
    src/drawbox.cpp \
    src/drawaxes.cpp \
    src/ball.cpp \
    src/aabbox.cpp \
    src/octree.cpp \
    src/state.cpp

QT += \
    opengl \
    script
