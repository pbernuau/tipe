TIPE 2010 - 2011
================
Mobilité, Mouvement
=====================
Conception d'un moteur physique
-------------------------------

- - - 

# Résumé

La simulation des phénomènes mécaniques sur ordinateur est un domaine de l'informatique en
expansion. Le **calcul des déplacements d'objets dans un environnement virtuel**, qui s'inscrit dans
le thème de la mobilité et du mouvement, a, en effet, de très nombreuses utilités. Ces programmes
qui calculent les déplacements sont souvent conçus de manière personnalisée pour se conformer aux
contraintes de leur emploi. Ils sont communément appelés "moteurs physiques".

J'ai souhaité m'intéresser à l'élaboration en C++ d'un moteur physique. Ce langage a été choisi en
raison de son exécution très rapide et parce qu'il existe de très nombreuses bibliothèques
facilitant le chargement des scènes, leur affichage, et la capture d'images.

Le programme final gère deux types d'objets :

+ des sphères homogènes,
+ des plans limités par les faces d'un parallélépipède aligné sur le repère cartésien.

**Le principal problème de réalisation réside dans la gestion des collisions entre les différents
objets.**

- - - 

# Plan de la présentation

1. **Présentation du projet** : Définition, utilité et problèmes liés à la réalisation d'un moteur
physique.
2. **Mise en oeuvre** : Modèles choisis, fonctionnement général du moteur, techniques d'optimisation
et d'amélioration
	employées.
3. **Analyse du programme** : Difficultés rencontrées, défauts du modèle, améliorations possibles,
comparaison avec d'autres moteurs.

- - - 

# Sources

- [Basic Game Physics](http://ai.eecs.umich.edu/soar/Classes/494/talks/Lecture%205%20Basic%20Physics.pdf)
- [Fast, Accurate Collision Detection Between Circles or Spheres](http://www.gamasutra.com/view/feature/3015/pool_hall_lessons_fast_accurate_.php?page=2)
- [Conception d'un moteur physique](http://gregorycorgie.developpez.com/tutoriels/physic/)
- [Bouncing Balls Guide](http://grom358.mygamesonline.org/bouncingballs_guide.html)
- [Newton Game Dynamics - Collision primitives](http://newtondynamics.com/wiki/index.php5?title=Collision_primitives)

